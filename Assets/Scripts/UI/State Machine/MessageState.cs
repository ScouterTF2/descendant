using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

// Message state, shows message view
public class MessageState : BaseState
{
	// Reference to item pickup component of player
	[SerializeField] ItemPickup itemArm;

	// Current message text
	public List<string> messageText;

	// Currently shown line
	int line;

	// Reference to currently active coroutine
	IEnumerator coroutine;

	// Variable to check if line is fully shown yet or not
	bool lineDone;

	public override void PrepareState()
	{
		base.PrepareState();

		// Disable player's movement, sound and laser
		owner.PlayerController.DisableMovement();
		owner.PlayerController.DisableSound();
		owner.PlayerController.DisableLaser();

		// Set line variable to indicate first line of message text
		line = 0;

		// Set DisplayMessageLineOverTime as active coroutine and start it
		coroutine = DisplayMessageLineOverTime();
		owner.PlayerController.StartCoroutine(coroutine);

		// Show Message view
		owner.UI.MessageView.ShowView();
	}

	public override void DestroyState()
	{
		// Hide Message view
		owner.UI.MessageView.HideView();

		// Clear message text in UI object
		owner.UI.MessageView.message.SetText("");

		// Enable player's movement, sound and laser
		owner.PlayerController.EnableMovement();
		owner.PlayerController.EnableSound();
		owner.PlayerController.EnableLaser();

		// Drop message item
		owner.PlayerController.ItemPickup.DropItem();

		base.DestroyState();
	}

	public override void UpdateState()
	{
		base.UpdateState();

		// If Submit button is pressed
		if (Input.GetButtonDown("Submit"))
		{
			// If coroutine has iterated the whole line
			if (lineDone)
			{
				// If there are remaining lines of message text
				if (line < messageText.Count)
				{
					// Clear UI message text
					owner.UI.MessageView.message.SetText("");

					// Set DisplayMessageLineOverTime as active coroutine and start it
					coroutine = DisplayMessageLineOverTime();
					owner.PlayerController.StartCoroutine(coroutine);
				}
				// Otherwise, invoke close function
				else
				{
					CloseClicked();
				}
			}
			// If coroutine is still in progress
			else
			{
				// Stop active coroutine
				owner.PlayerController.StopCoroutine(coroutine);

				// Display whole line at once
				DisplayMessageLineInstantly();
			}
		}

		// If Cancel button is pressed, invoke close function
		if (Input.GetButtonDown("Cancel"))
		{
			CloseClicked();
		}
	}

	IEnumerator DisplayMessageLineOverTime()
	{
		// Set lineDone variable to false
		lineDone = false;

		// Disable both indicators
		owner.UI.MessageView.skipIndicator.gameObject.SetActive(false);
		owner.UI.MessageView.endIndicator.gameObject.SetActive(false);

		// For every character in message text line
		for (int i = 0; i < messageText[line].Length; i++)
		{
			yield return new WaitForSeconds(0.05f);

			// Add currently iterated character of message text line to UI message text object
			owner.UI.MessageView.message.SetText(owner.UI.MessageView.message.text + messageText[line][i]);
		}

		// Set lineDone variable to true
		lineDone = true;

		// Go to next line
		line++;

		// Show appropiate indicator
		SetIndicator();

		yield return null;
	}

	void DisplayMessageLineInstantly()
	{
		// Set whole message text line as text in UI message text object
		owner.UI.MessageView.message.SetText(messageText[line]);

		// Go to next line
		line++;

		// Show appropiate indicator
		SetIndicator();

		// Set lineDone variable to true
		lineDone = true;
	}

	void CloseClicked()
	{
		// Stop active coroutine if any is active
		if (coroutine != null)
		{
			owner.PlayerController.StopCoroutine(coroutine);
		}

		// Clear UI message text
		owner.UI.MessageView.message.SetText("");

		// Set current state to Game state
		owner.ChangeState(new GameState());
	}

	void SetIndicator()
	{
		// If all lines of text have been indicated, show message end indicator
		if (line == messageText.Count - 1)
		{
			owner.UI.MessageView.endIndicator.gameObject.SetActive(true);
		}
		// Otherwise, show line skip indicator
		else
		{
			owner.UI.MessageView.skipIndicator.gameObject.SetActive(true);
		}
	}
}
