using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISlider : UIInputElement
{
	// Reference to slider object
	[SerializeField] public Slider slider;
}
