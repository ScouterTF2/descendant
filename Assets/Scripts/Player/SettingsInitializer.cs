using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SettingsInitializer : MonoBehaviour
{
	// Variables for setting initial music and effects volume values
	[Range(0f, 1f)] public float musicVolume = 1f;
	[Range(0f, 1f)] public float effectsVolume = 1f;

	// Variables for setting initial field of view value
	[Range(30f, 120f)] public float fieldOfView = 60f;

	// Variables for setting initial VSync value
	[Range(0, 1)] public int vSync = 1;

	void Awake()
	{
		// Initialise settings
		SetInitialSettings();
	}

	public void SetInitialSettings()
	{
		// If music volume is not set in player prefs, set its initial value
		if (!PlayerPrefs.HasKey("musicVolume"))
		{
			PlayerPrefs.SetFloat("musicVolume", musicVolume);
		}

		// If effects volume is not set in player prefs, set its initial value
		if (!PlayerPrefs.HasKey("effectsVolume"))
		{
			PlayerPrefs.SetFloat("effectsVolume", effectsVolume);
		}

		// If field of view is not set in player prefs, set its initial value
		if (!PlayerPrefs.HasKey("fieldOfView"))
		{
			PlayerPrefs.SetFloat("fieldOfView", fieldOfView);
		}

		// If resolution values not set in player prefs, set their initial values
		if (!PlayerPrefs.HasKey("resolutionWidth") || !PlayerPrefs.HasKey("resolutionHeight") || !PlayerPrefs.HasKey("resolutionRefreshRate"))
		{
			PlayerPrefs.SetInt("resolutionWidth", Screen.currentResolution.width);
			PlayerPrefs.SetInt("resolutionHeight", Screen.currentResolution.height);
			PlayerPrefs.SetInt("resolutionRefreshRate", Screen.currentResolution.refreshRate);
		}

		// If VSync state is not set in player prefs, set its initial state
		if (!PlayerPrefs.HasKey("vSync"))
		{
			PlayerPrefs.SetInt("vSync", vSync);
		}

		// Save all player prefs
		PlayerPrefs.Save();
	}
}
