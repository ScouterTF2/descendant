using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageKeyHolder : MonoBehaviour
{
	// Current message identificator
	[SerializeField] public string keyName;
}
