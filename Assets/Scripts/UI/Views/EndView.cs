using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// Pause view class, passes button events
public class EndView : BaseView
{
    // Events to attach to
    public UnityAction OnMenuClicked;

	// List of selectable buttons for keyboard/gamepad functionality
	[SerializeField] UIButton button;
    public UIButton Button => button;

    // Method called by Menu Button
    public void MenuClick()
    {
        // Invoke action set to Menu button
        OnMenuClicked?.Invoke();
    }

	// Method to display selection background for d button
	public void selectButton()
	{
		// Activate button's selection background
		Button.selection.gameObject.SetActive(true);

        // Find event system object and set button as selected button in event system
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(Button.button.gameObject);
	}
}
