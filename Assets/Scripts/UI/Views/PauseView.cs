using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// Pause view class, passes button events
public class PauseView : BaseView
{
    // Events to attach to
    public UnityAction OnResumeClicked;
    public UnityAction OnMenuClicked;
    public UnityAction OnOptionsClicked;

	// List of selectable buttons for keyboard/gamepad functionality
	[SerializeField] List<UIButton> buttons;
    public List<UIButton> Buttons => buttons;

	// Reference to object of currently selected button
	UIButton currentlySelectedButton;
	public UIButton CurrentlySelectedButton => currentlySelectedButton;

    // Method called by Resume Button
    public void ResumeClick()
    {
        // Invoke action set to Resume button
        OnResumeClicked?.Invoke();
    }

	// Method called by Options Button
    public void OptionsClick()
    {
        // Invoke action set to Options button
        OnOptionsClicked.Invoke();
    }

    // Method called by Menu Button
    public void MenuClick()
    {
        // Invoke action set to Menu button
        OnMenuClicked?.Invoke();
    }

	// Method to display selection background for currently selected button
	public void selectButton(int index)
	{
		// If any button is currently selected, deactivate its selection background
		if (currentlySelectedButton != null) 
		{
			currentlySelectedButton.selection.gameObject.SetActive(false);
		}

		// Set specified button as currently selected button and activate its selection background
		currentlySelectedButton = Buttons[index];
		currentlySelectedButton.selection.gameObject.SetActive(true);

        // Find event system object and set specified button as selected button in event system
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(currentlySelectedButton.button.gameObject);
	}
}
