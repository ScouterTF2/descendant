using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler : MonoBehaviour
{
    // Audio component
    AudioSource audioSource;

    // Reference to audio file for collision
    [SerializeField] AudioClip collisionSound;

    // Reference to player controller
    PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        // Get audio source component
        audioSource = GetComponent<AudioSource>();

        // Get player controller component
        playerController = GetComponent<PlayerController>();
    }

    // OnCollisionEnter is called when object collides with another object
    void OnCollisionEnter(Collision other)
    {
        // Stop any sounds playing from audio source
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        
        // If sound is enabled, play collision sound
        if (playerController.Sound)
        {
            audioSource.PlayOneShot(collisionSound);
        }
    }
}
