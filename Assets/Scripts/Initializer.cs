using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initializer : MonoBehaviour
{
	[SerializeField] StateMachine stateMachine;

    // Start is called before the first frame update
    void Start()
    {
        // Activate state machine object
        stateMachine.gameObject.SetActive(true);

        // Destroy initializer
        Destroy(this.gameObject);
    }
}
