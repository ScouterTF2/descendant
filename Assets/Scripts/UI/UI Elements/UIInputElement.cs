using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class UIInputElement : MonoBehaviour
{
	// Reference to selection background
	public Image selection;
}
