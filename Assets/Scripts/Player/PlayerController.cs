using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Reference to player's Rigidbody
    Rigidbody rigidBody;
    public Rigidbody RigidBody => rigidBody;

    // References to all of player's audio sources (main source for engine and other sources)
    AudioSource playerSource;
    AudioSource[] childrenSources;

    // Reference to current level's level manager
    public LevelManager levelManager;

    // Reference to player's cameras
    [SerializeField] Camera mainCamera;
    [SerializeField] Camera glassViewCamera;
    public Camera MainCamera => mainCamera;
    public Camera GlassViewCamera => glassViewCamera;

    // Reference to player's shader quad used in glass view
    [SerializeField] MeshRenderer shaderQuad;
    public MeshRenderer ShaderQuad => shaderQuad;
    
    // Reference to player's item arm component
    [SerializeField] ItemPickup itemPickup;
    public ItemPickup ItemPickup => itemPickup;

    // Reference to player's laser shoot component
    [SerializeField] LaserShoot laserShoot;

    // Variable used to toggle applying movement and rotation to player
    bool movement = false;
    public bool Movement => movement;

    // Variable used to toggle enabling and disabling sound
	bool sound = false;
	public bool Sound => sound;

    // Start is called before the first frame update
    void Start()
    {
        // Set screen resolution to resolution set in player prefs
        Screen.SetResolution(PlayerPrefs.GetInt("resolutionWidth"), PlayerPrefs.GetInt("resolutionHeight"), FullScreenMode.FullScreenWindow);

        // Set camera's field of view to value set in player prefs
        SetFOV();

        // Set VSync state to state set in player prefs
        SetVSync();

        // Get player's Rigidbody
        rigidBody = this.gameObject.GetComponent<Rigidbody>();

        // Get player's audio sources
        playerSource = this.gameObject.GetComponent<AudioSource>();
        childrenSources = this.gameObject.GetComponentsInChildren<AudioSource>();
    }

    public void DisableMovement()
    {
        // Set movement variable to false
        movement = false;
    }

    public void EnableMovement()
    {
        // Set movement variable to true
        movement = true;
    }

    public void DisableSound()
    {
		// Stop sound on main audio source
        if (playerSource.isPlaying)
        {
            playerSource.Stop();
        }
		
		// Stop sound on other audio sources
		foreach (AudioSource childrenSource in childrenSources)
        {
            if (childrenSource.isPlaying)
            {
                childrenSource.Stop();
            }
        }

		// Set sound variable to false
		sound = false;
    }

    public void EnableSound()
    {
		// Set sound variable to true
		sound = true;
    }

    public void DisableLaser()
    {
        // Disable laser shoot component
        laserShoot.enabled = false;
    }

    public void EnableLaser()
    {
        // Enable laser shoot component
        laserShoot.enabled = true;
    }

    public void SetFOV()
    {
        // Set field of view of both cameras to value from argument
        mainCamera.fieldOfView = PlayerPrefs.GetFloat("fieldOfView");
		glassViewCamera.fieldOfView = PlayerPrefs.GetFloat("fieldOfView");
	}

    public void SetVSync()
    {
        // Set VSync state based on value of vSync variable in player prefs
        QualitySettings.vSyncCount = PlayerPrefs.GetInt("vSync");
    }

    public void ResetVelocity()
    {
        // Set player's velocity to zero
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;
    }

    public void TeleportToSpawn()
    {
        // If player is currently in a level, set his position to position of spawn location
        if (levelManager != null)
        {
            this.transform.position = levelManager.SpawnLocation.transform.position;
            this.transform.rotation = levelManager.SpawnLocation.transform.rotation;
        }
    }
}
