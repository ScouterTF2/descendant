using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	// Speed of rotation
	[SerializeField] float rotationSpeed = 200f;

	// Force multiplier for thrusting in any direction
	[SerializeField] float thrustForce = 10000f;

	// Maximum speed of player
	[SerializeField] float maxSpeed = 200f;

	// Velocity value below which player will be stopped
	[SerializeField] float stopSpeed = 0.1f;

	// Reference to player's Rigidbody
	Rigidbody rigidBody;

	// Audio component used to play thrust sound
	AudioSource audioSource;

	// Reference to player controller
	PlayerController playerController;

	// Start is called before the first frame update
	void Start()
	{
		// Get player's Rigidbody
		rigidBody = GetComponent<Rigidbody>();

		// Get audio source component
		audioSource = GetComponent<AudioSource>();

		// Get player controller component
		playerController = GetComponent<PlayerController>();
	}

	// Update is called once per frame
	void Update()
	{
		// Current state of inputs for movement
		float xMovement = Input.GetAxis("Horizontal");
		float yMovement = Input.GetAxis("Vertical");
		float zMovement = Input.GetAxis("Accelerate");

		// Current state of inputs for rotation
		float xRotation = Input.GetAxis("LookVertical");
		float yRotation = Input.GetAxis("LookHorizontal");
		float zRotation = Input.GetAxis("Barrel");

		// Vector with movement input states
		Vector3 movementVector = new Vector3(xMovement, yMovement, zMovement);

		// Vector with rotation input states
		Vector3 rotationVector = new Vector3(xRotation, yRotation, zRotation);

		// If player's movement is enabled
		if (playerController.Movement)
		{
			// Add force to player using input
			ApplyMovement(movementVector);

			// Rotate player using input
			ApplyRotation(rotationVector);
		}

		// If player's sound is enabled
		if (playerController.Sound)
		{
			// Play thrust sound
			PlaySound(movementVector);
		}

		// Limit velocity of player
		SetMaxSpeed();

		// Stop the player when velocity is below certain value
		Stop(movementVector);
	}

	void ApplyRotation(Vector3 rotationVector)
	{
		// Rotate player using input and rotation speed and make the calculation independent from framerate
		transform.Rotate(rotationVector * rotationSpeed * Time.deltaTime);
	}

	void ApplyMovement(Vector3 movementVector)
	{
		// Add force to player (make it move) using input and move speed and make the calculation independent from framerate
		rigidBody.AddRelativeForce(movementVector * thrustForce * Time.deltaTime);
	}

    void PlaySound(Vector3 movementVector)
    {
        // If not enough movement input is being sent, stop the thrust sound
        if (movementVector[0] <= 0.1 && movementVector[0] >= -0.1 && movementVector[1] <= 0.1 && movementVector[1] >= -0.1 && movementVector[2] <= 0.1 && movementVector[2] >= -0.1)
        {
            audioSource.Stop();
        }
        // In other case, play thrust sound if it isn't already playing
        else
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
    }

	void SetMaxSpeed()
	{
		// Normalize player's velocity and multiply it by max speed so that player cannot exceed that speed
		rigidBody.velocity = Vector3.ClampMagnitude(rigidBody.velocity, maxSpeed);
	}

    void Stop(Vector3 movementVector)
    {
        // Stop the player when speed is below stop speed and not enough input is being sent
        if (movementVector[0] <= 0.1 && movementVector[0] >= -0.1 && movementVector[1] <= 0.1 && movementVector[1] >= -0.1 && movementVector[2] <= 0.1 && movementVector[2] >= -0.1 && rigidBody.velocity.magnitude > -stopSpeed && rigidBody.velocity.magnitude < stopSpeed)
        {
            rigidBody.velocity = Vector3.zero;
        }
    }
}
