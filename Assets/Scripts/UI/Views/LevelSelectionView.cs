using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// Loading level view class, passes button events
public class LevelSelectionView : BaseView
{
	// Events to attach to
	public UnityAction OnReturnClicked;
	public UnityAction<int> OnLevelClicked;

	// Reference to list of buttons for all levels
	[SerializeField] List<UIButton> buttons;
    public List<UIButton> Buttons => buttons;

	// Reference to object of currently selected button
	UIButton currentlySelectedButton;
	public UIButton CurrentlySelectedButton => currentlySelectedButton;

	// Method called by Level buttons
	public void LevelClick(int level)
	{
		// Invoke action set to Level buttons with a level number as an argument
		OnLevelClicked?.Invoke(level);
	}

	// Method called by Return button
	public void ReturnClick()
	{
		// Invoke action set to Return button
		OnReturnClicked?.Invoke();
	}

	// Method to display selection background for currently selected button
	public void selectButton(int index)
	{
		// If any button is currently selected, deactivate its selection background
		if (currentlySelectedButton != null) 
		{
			currentlySelectedButton.selection.gameObject.SetActive(false);
		}

		// Set specified button as currently selected button and activate its selection background
		currentlySelectedButton = Buttons[index];
		currentlySelectedButton.selection.gameObject.SetActive(true);

		// Find event system object and set specified button as selected button in event system
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(currentlySelectedButton.button.gameObject);
	}
}
