using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

// Loading level state, shows loading level view
public class LoadingLevelState : BaseState
{
    // Level which will be loaded when loading screen is shown
    public int levelToLoad;

    // Variable used for determining if Loading Level state was invoked from Game state
    public bool jumpFromGame = false;

    public override void PrepareState()
    {
        base.PrepareState();

        // Show Game view
        owner.UI.LoadingLevelView.ShowView();
        
        // If the state was changed to Loading Level from Game state (when finishing level), disable player's movement, sound and laser
        if (jumpFromGame)
        {
            owner.PlayerController.DisableMovement();
            owner.PlayerController.DisableSound();
            owner.PlayerController.DisableLaser();
        }

        // If there are no more levels, load end state
		if (levelToLoad == 0)
		{
			owner.ChangeState(new EndState());
		}
        // Otherwise, start coroutine which loads next level
		else
		{
			owner.PlayerController.StartCoroutine(LoadNewScene());
		}
    }

    public override void DestroyState()
    {
        // Enable player's movement, sound and laser
        owner.PlayerController.EnableMovement();
        owner.PlayerController.EnableSound();
        owner.PlayerController.EnableLaser();

		// Hide Loading Level view
		owner.UI.LoadingLevelView.HideView();

		base.DestroyState();
    }

    IEnumerator LoadNewScene()
    {
        // Initiate asynchronous scene loading for level specified in variable
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Level" + levelToLoad, LoadSceneMode.Additive);

		// Set achieved level in save data to current level
		owner.PlayerController.gameObject.GetComponent<ProgressProcessor>().UpdateCurrentLevel(levelToLoad);

		// Return null until level is loaded
		while (!asyncLoad.isDone)
        {
            yield return null;
        }

        // Load level's level manager after it is loaded
        yield return owner.PlayerController.StartCoroutine(LoadLevelManager());
        
        // Set loaded level as current level
        owner.CurrentLevel = levelToLoad;

		// Set current state to Game state
		owner.ChangeState(new GameState());

        // Disable laser in level if it is a level with laser disabled at start
		if (!owner.PlayerController.levelManager.LaserEnabled)
		{
            owner.PlayerController.DisableLaser();
		}

        // Set player's velocity to zero
        owner.PlayerController.ResetVelocity();
    }

    IEnumerator LoadLevelManager()
    {
        // Find level's level manager and set it to player controller
        owner.PlayerController.levelManager = GameObject.Find("Level Manager").GetComponent<LevelManager>();

        // Teleport player to spawn point
        owner.PlayerController.TeleportToSpawn();

        yield return null;
    }
}
