using UnityEngine;

// Class used for providing simples methods for UI views.
public class BaseView : MonoBehaviour
{
    // Method called to show view
    public virtual void ShowView()
    {
        // Enable object holding view
        gameObject.SetActive(true);
    }

    // Method called to hide view
    public virtual void HideView()
    {
        // Disable object holding view
        gameObject.SetActive(false);
    }
}
