using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SaveData
{
	// Number of last finished level
	public int levelAchieved;

	// Number of currently played level
	public int currentLevel;
}
