using UnityEngine;
using UnityEngine.SceneManagement;

// End state, shows end view after finishing game
public class EndState : BaseState
{
	// Reference to number of currently selected button from buttons list
	int currentButtonNumber;

    public override void PrepareState()
    {
        base.PrepareState();

        // Attach functions to view events
        owner.UI.EndView.OnMenuClicked += MenuClicked;

		// Set menu button as current button and select it
		owner.UI.EndView.selectButton();

		// Disable player controls and sound
		owner.PlayerController.DisableMovement();
		owner.PlayerController.DisableSound();
		owner.PlayerController.DisableLaser();

		// Reset player's velocity
		owner.PlayerController.ResetVelocity();

		// Load Level 3 scene used in menu
		SceneManager.LoadScene("Level3", LoadSceneMode.Additive);
		owner.PlayerController.transform.position = new Vector3(8.66f, -1.47f, -32.22f);
		owner.PlayerController.transform.rotation = new Quaternion(0, 0, 0, 1);

		// Show End view
		owner.UI.EndView.ShowView();
    }

	public override void DestroyState()
    {
        // Hide End view
        owner.UI.EndView.HideView();

        // Detach functions from view events
        owner.UI.EndView.OnMenuClicked -= MenuClicked;

		// Find event system object and remove its button/element selection
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(null);

        base.DestroyState();
    }

	public override void UpdateState()
	{
		base.UpdateState();

		// Rotate player so that he rotates with a certain speed around Y axis, independent from framerate
		owner.PlayerController.transform.Rotate(0, 10 * Time.deltaTime, 0);

	}

	// Function called when Menu button is clicked in End view
	void MenuClicked()
    {
        // Set current state to Menu state
        owner.ChangeState(new MenuState { loadMenuContent = false, jumpFromSubmenu = true });
    }
}