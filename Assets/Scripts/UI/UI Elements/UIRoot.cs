using UnityEngine;

// UI Root class, used for storing references to UI views
public class UIRoot : MonoBehaviour
{
    // Reference to Menu view
    [SerializeField] MenuView menuView;
    public MenuView MenuView => menuView;

    // Reference to Game view
    [SerializeField] GameView gameView;
    public GameView GameView => gameView;

    // Reference to Pause view
    [SerializeField] PauseView pauseView;
    public PauseView PauseView => pauseView;

    // Reference to Options view
    [SerializeField] OptionsView optionsView;
    public OptionsView OptionsView => optionsView;

    // Reference to Loading Level view
    [SerializeField] LoadingLevelView loadingLevelView;
    public LoadingLevelView LoadingLevelView => loadingLevelView;

    // Reference to Level Selection view
	[SerializeField] LevelSelectionView levelSelectionView;
	public LevelSelectionView LevelSelectionView => levelSelectionView;

    // Reference to Message view
	[SerializeField] MessageView messageView;
	public MessageView MessageView => messageView;

	// Reference to End view
	[SerializeField] EndView endView;
	public EndView EndView => endView;

	// Reference to Controls view
	[SerializeField] ControlsView controlsView;
	public ControlsView ControlsView => controlsView;
}
