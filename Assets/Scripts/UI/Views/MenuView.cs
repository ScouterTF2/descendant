using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// Menu view class, passes button events
public class MenuView : BaseView
{
    // Events to attach to
    public UnityAction OnStartClicked;
	public UnityAction OnContinueClicked;
	public UnityAction OnLevelsClicked;
	public UnityAction OnControlsClicked;
    public UnityAction OnOptionsClicked;
    public UnityAction OnQuitClicked;

	// List of selectable buttons for keyboard/gamepad functionality
	[SerializeField] List<UIButton> buttons;
    public List<UIButton> Buttons => buttons;

	// Reference to object of currently selected button
	UIButton currentlySelectedButton;
	public UIButton CurrentlySelectedButton => currentlySelectedButton;

	// Method called by Start button
	public void StartClick()
    {
        // Invoke action set to Start button
        OnStartClicked?.Invoke();
    }

	// Method called by Continue button
	public void ContinueClick()
	{
        // Invoke action set to Continue button
		OnContinueClicked?.Invoke();
	}

	// Method called by Levels button
	public void LevelsClick()
	{
        // Invoke action set to Levels button
		OnLevelsClicked?.Invoke();
	}

	// Method called by Controls button
	public void ControlsClick()
	{
		OnControlsClicked?.Invoke();
	}

    // Method called by Options button
    public void OptionsClick()
    {
        // Invoke action set to Options button
        OnOptionsClicked?.Invoke();
    }

    // Method called by Quit button
    public void QuitClick()
    {
        // Invoke action set to Quit button
        OnQuitClicked?.Invoke();
    }

	// Method to display selection background for currently selected button
	public void selectButton(int index)
	{
		// If any button is currently selected, deactivate its selection background
		if (currentlySelectedButton != null) 
		{
			currentlySelectedButton.selection.gameObject.SetActive(false);
		}

		// Set specified button as currently selected button and activate its selection background
		currentlySelectedButton = Buttons[index];
		currentlySelectedButton.selection.gameObject.SetActive(true);

		// Find event system object and set specified button as selected button in event system
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(currentlySelectedButton.button.gameObject);
	}
}
