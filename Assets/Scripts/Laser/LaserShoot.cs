using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserShoot : MonoBehaviour
{
    // Particle system component from laser object
    ParticleSystem laser;

    // Time between subsequent shots in seconds (laser cannot be shot during that time)
    [SerializeField] float rechargeTime = 1;

    // Variable used to determine current time since last shot
    float rechargeTimer = 1;

    // Audio component used to play laser sound
    AudioSource audioSource;

    // Reference to player controller
    [SerializeField] PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        // Get particle system of laser
        laser = GetComponent<ParticleSystem>();

        // Get audio source component
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // If a fire button is pressed
        if (Input.GetButtonDown("Fire1") && rechargeTimer >= rechargeTime)
        {
            // Emit laser particle (shoot laser)
            laser.Emit(1);
            
            // Reset recharge times
            rechargeTimer = 0;

            // Stop any sounds playing from audio source
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
        
            // If sound is enabled, play laser sound
            if (playerController.Sound)
            {
                audioSource.Play();
            }
        }

        // If amount of time equal to recharge time has not yet passed since last shot
        if (rechargeTime >= rechargeTimer)
        {
            // Add amount of time equivalent to amount of time current frame is shown on screen to recharge timer
            rechargeTimer += Time.deltaTime;
        }
    }
}
