using UnityEngine;
using System;
using System.Collections.Generic;

// Menu state, shows Menu view with background based on which state it has changed from and adds user interaction with that view
public class OptionsState : BaseState
{
	// An enumerator variable storing possible states from which the Options state can be started
	public enum ReturnToStates
	{
		Menu,
		Pause
	}

	// Variable used for determining from which state the Options state was changed to
	public ReturnToStates returnToState = ReturnToStates.Menu;

	// Reference to number of currently selected element from elements list
	int currentElementNumber;

	// A percentage of slider's interval used to increment or decrement its value
	float sliderIncrementValue = 0.1f;

	// List of resolutions available on player's machine
	List<String> resolutions;
	public List<String> Resolutions => resolutions;

	// Index of currently set resolution in list
	int currentlySelectedResolution;
	public int DefaultResolution => currentlySelectedResolution;

	public override void PrepareState()
	{
		base.PrepareState();

		// Attach functions to view events
		owner.UI.OptionsView.OnReturnClicked += ReturnClicked;
		owner.UI.OptionsView.OnMusicVolumeChanged += MusicVolumeChanged;
		owner.UI.OptionsView.OnEffectsVolumeChanged += EffectsVolumeChanged;
		owner.UI.OptionsView.OnFOVChanged += FOVChanged;
		owner.UI.OptionsView.OnPreviousResolutionClicked += PreviousResolutionClicked;
		owner.UI.OptionsView.OnNextResolutionClicked += NextResolutionClicked;
		owner.UI.OptionsView.OnVSyncChanged += VSyncChanged;

		// Change position of slider handles based on what are their respective options set to in player prefs
		owner.UI.OptionsView.MusicVolumeSlider.value = PlayerPrefs.GetFloat("musicVolume");
		owner.UI.OptionsView.EffectsVolumeSlider.value = PlayerPrefs.GetFloat("effectsVolume");
		owner.UI.OptionsView.FOVSlider.value = PlayerPrefs.GetFloat("fieldOfView");

		// Change value of slider value texts based on current position of their respective slider handles 
		owner.UI.OptionsView.MusicVolumeValueText.text = (Math.Round(owner.UI.OptionsView.MusicVolumeSlider.value * 100)).ToString();
		owner.UI.OptionsView.EffectsVolumeValueText.text = (Math.Round(owner.UI.OptionsView.EffectsVolumeSlider.value * 100)).ToString();
		owner.UI.OptionsView.FOVValueText.text = owner.UI.OptionsView.FOVSlider.value.ToString();

		// Initialise resolution list
		resolutions = new List<String>();

		// For every resolution compatible with player's screen
		for (int i = 0; i < Screen.resolutions.Length; i++)
		{
			// Add currently iterated resolution to list
			resolutions.Add(Screen.resolutions[i].ToString());

			// If current resolution is iterated, set it as currently selected and set text of resolution text object
			if (Screen.resolutions[i].width == PlayerPrefs.GetInt("resolutionWidth") && Screen.resolutions[i].height == PlayerPrefs.GetInt("resolutionHeight") && Screen.resolutions[i].refreshRate == PlayerPrefs.GetInt("resolutionRefreshRate"))
			{
				currentlySelectedResolution = i;

				owner.UI.OptionsView.ResolutionValue.text = resolutions[currentlySelectedResolution];
			}
		}

		// Change state of VSync toggle based on what is VSync state set to in player prefs
		owner.UI.OptionsView.VSyncToggle.isOn = PlayerPrefs.GetInt("vSync") == 1;

		// Set first element in list as current element and select it
		currentElementNumber = 0;
		owner.UI.OptionsView.selectElement(currentElementNumber);

		// Show Options view
		owner.UI.OptionsView.ShowView();
	}

	public override void DestroyState()
	{
		// Save current player configuration
		PlayerPrefs.Save();

		// Hide Options view
		owner.UI.OptionsView.HideView();

		// Detach functions from view events
		owner.UI.OptionsView.OnReturnClicked -= ReturnClicked;
		owner.UI.OptionsView.OnMusicVolumeChanged -= MusicVolumeChanged;
		owner.UI.OptionsView.OnEffectsVolumeChanged -= EffectsVolumeChanged;
		owner.UI.OptionsView.OnFOVChanged -= FOVChanged;
		owner.UI.OptionsView.OnPreviousResolutionClicked -= PreviousResolutionClicked;
		owner.UI.OptionsView.OnNextResolutionClicked -= NextResolutionClicked;
		owner.UI.OptionsView.OnVSyncChanged -= VSyncChanged;

		// Find event system object and remove its button/element selection
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(null);

		// Change game resolution to newly set resolution
		Screen.SetResolution(PlayerPrefs.GetInt("resolutionWidth"), PlayerPrefs.GetInt("resolutionHeight"), FullScreenMode.FullScreenWindow);

		base.DestroyState();
	}

	public override void UpdateState()
	{
		base.UpdateState();

		// If Menu was previous state, rotate player analogically to Menu state
		if (returnToState == ReturnToStates.Menu)
		{
			owner.PlayerController.transform.Rotate(0, 10 * Time.deltaTime, 0);
		}

		// Handle states of UI key presses
		HandleDownClicked();
		HandleUpClicked();
		HandleRightClicked();
		HandleLeftClicked();
		HandleCancelClicked();
	}

	void HandleDownClicked()
	{
		// If Menu Control Down button is pressed
		if (Input.GetButtonDown("MenuControlDown"))
		{
			// If current element is the last element in list, set first element in list as current element and select it
			if (currentElementNumber == owner.UI.OptionsView.Elements.Count - 1)
			{
				currentElementNumber = 0;
				owner.UI.OptionsView.selectElement(currentElementNumber);
			}
			// Otherwise, set next element in list as current element and select it
			else
			{
				currentElementNumber++;
				owner.UI.OptionsView.selectElement(currentElementNumber);
			}
		}
	}

	void HandleUpClicked()
	{
		// If Menu Control Up button is pressed
		if (Input.GetButtonDown("MenuControlUp"))
		{
			// If current button is the first button in list, set last button in list as current button and select it
			if (currentElementNumber == 0)
			{
				currentElementNumber = owner.UI.OptionsView.Elements.Count - 1;
				owner.UI.OptionsView.selectElement(currentElementNumber);
			}
			// Otherwise, set previous button in list as current button and select it
			else
			{
				currentElementNumber--;
				owner.UI.OptionsView.selectElement(currentElementNumber);
			}
		}
	}

	void HandleLeftClicked()
	{
		// If Menu Control Left button is pressed
		if (Input.GetButtonDown("MenuControlLeft"))
		{
			// If currently selected option is a slider, decrease its value by calculated increment
			if (owner.UI.OptionsView.CurrentlySelectedElement is UISlider)
			{
				var tmp = (UISlider) owner.UI.OptionsView.CurrentlySelectedElement;
				tmp.slider.value -= CalculateSliderIncrement(tmp.slider.minValue, tmp.slider.maxValue);
			}
			// If currently selected option is a resolution text, invoke previous resolution function
			else if (owner.UI.OptionsView.CurrentlySelectedElement is UIResolution)
			{
				PreviousResolutionClicked();
			}
		}
	}

	void HandleRightClicked()
	{
		// If Menu Control Right button is pressed
		if (Input.GetButtonDown("MenuControlRight"))
		{
			// If currently selected option is a slider, increase its value by calculated increment
			if (owner.UI.OptionsView.CurrentlySelectedElement is UISlider)
			{
				var tmp = (UISlider) owner.UI.OptionsView.CurrentlySelectedElement;
				tmp.slider.value += CalculateSliderIncrement(tmp.slider.minValue, tmp.slider.maxValue);
			}
			// If currently selected option is a resolution text, invoke next resolution function
			else if (owner.UI.OptionsView.CurrentlySelectedElement is UIResolution)
			{
				NextResolutionClicked();
			}
		}
	}

	void HandleCancelClicked()
	{
		// If Cancel button is pressed, invoke return function
		if (Input.GetButtonDown("Cancel"))
		{
			ReturnClicked();
		}
	}

	// Function called when Return button is clicked in Options view
	void ReturnClicked()
	{
		// A switch instruction which sets current state based on which state was previous
		switch (returnToState)
		{
			// If Menu was previous state, change state to Menu with preventing loading menu content again and signalising that the state was invoked from a submenu
			case ReturnToStates.Menu:
				owner.ChangeState(new MenuState { loadMenuContent = false, jumpFromSubmenu = true });
				break;
			// If Pause was previous state, change state to Pause with signalising the state it was invoked from
			case ReturnToStates.Pause:
				owner.ChangeState(new PauseState { jumpFromOptions = true });
				break;
		}
	}

	// Function called when Music Volume slider handle position is changed
	void MusicVolumeChanged()
	{
		// Change music volume in player prefs to current slider value
		PlayerPrefs.SetFloat("musicVolume", owner.UI.OptionsView.MusicVolumeSlider.value);

		// Set music volume value text to rounded integer value of current slider value
		owner.UI.OptionsView.MusicVolumeValueText.text = (Math.Round(owner.UI.OptionsView.MusicVolumeSlider.value * 100)).ToString();

		// If Pause was previous state
		if (returnToState == ReturnToStates.Pause)
		{
			// Find Music Player object (MusicPlayerHandler)
			MusicPlayerHandler musicPlayer = GameObject.FindObjectOfType<MusicPlayerHandler>();
			// Set music volume in Music Player based on music volume in player prefs
			musicPlayer.SetVolume();
		}
	}

	// Function called when Effects Volume slider handle position is changed
	void EffectsVolumeChanged()
	{
		// Change effects volume in player prefs to current slider value
		PlayerPrefs.SetFloat("effectsVolume", owner.UI.OptionsView.EffectsVolumeSlider.value);

		// Set effects volume value text to rounded integer value of current slider value
		owner.UI.OptionsView.EffectsVolumeValueText.text = (Math.Round(owner.UI.OptionsView.EffectsVolumeSlider.value * 100)).ToString();

		// Find all objects with audio source except music player (AudioSourceHandler)
		AudioSourceHandler[] audioSources = GameObject.FindObjectsOfType<AudioSourceHandler>();

		// For each audio source except music player, set source volume based on effects volume in player prefs
		foreach (AudioSourceHandler audioSource in audioSources)
		{
			audioSource.SetVolume();
		}
	}

	// Function called when Field Of View slider handle position is changed
	void FOVChanged()
	{
		// Change field of view in player prefs to current slider value
		PlayerPrefs.SetFloat("fieldOfView", owner.UI.OptionsView.FOVSlider.value);

		// Set field of view value text to current slider value
		owner.UI.OptionsView.FOVValueText.text = owner.UI.OptionsView.FOVSlider.value.ToString();

		// Set camera's field of view to newly set value
		owner.PlayerController.SetFOV();

	}

	// Function called when Previous Resolution button is clicked in Options view
	void PreviousResolutionClicked()
	{
		// If currently selected resolution is not first in list, set previous resolution in list as currently selected resolution and set text of resolution text object
		if (currentlySelectedResolution > 0)
		{
			currentlySelectedResolution--;

			owner.UI.OptionsView.ResolutionValue.text = resolutions[currentlySelectedResolution];
		}

		// Invoke resolution save function
		SaveResolution();
	}

	// Function called when Next Resolution button is clicked in Options view
	void NextResolutionClicked()
	{
		// If currently selected resolution is not last in list, set next resolution in list as currently selected resolution and set text of resolution text object
		if (currentlySelectedResolution < resolutions.Count - 1)
		{
			currentlySelectedResolution++;

			owner.UI.OptionsView.ResolutionValue.text = resolutions[currentlySelectedResolution];
		}

		// Invoke resolution save function
		SaveResolution();
	}

	// Function called when VSync toggle state is changed
	void VSyncChanged()
	{
		// Change VSync state in player prefs to current toggle state
		PlayerPrefs.SetInt("vSync", owner.UI.OptionsView.VSyncToggle.isOn ? 1 : 0);

		// Set VSync to newly set state
		owner.PlayerController.SetVSync();
	}

	float CalculateSliderIncrement(float min, float max)
	{
		// Return an increment value that is a specified percentage of a subtraction of slider's minimum value from its maximum value
		return ((max - min) * sliderIncrementValue);
	}

	void SaveResolution()
	{
		// Change resolution in player prefs to newly chosen resolution
		PlayerPrefs.SetInt("resolutionWidth", Screen.resolutions[currentlySelectedResolution].width);
		PlayerPrefs.SetInt("resolutionHeight", Screen.resolutions[currentlySelectedResolution].height);
		PlayerPrefs.SetInt("resolutionRefreshRate", Screen.resolutions[currentlySelectedResolution].refreshRate);
	}
}
