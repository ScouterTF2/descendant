using System;
using UnityEngine;
using UnityEngine.SceneManagement;

// Level Selection state, shows Level Selection view and adds user interaction with that view
public class LevelSelectionState : BaseState
{
	// Variable used for destroying menu content
	public bool destroyMenuContent = true;

	// Variable used for enabling player's movement and sound when changing state to Game
	public bool jumpToGame = false;

	// Reference to number of currently selected button from buttons list
	int currentButtonNumber;

	// Number of currently unlocked levels
	int unlockedButtonCount;

	public override void PrepareState()
	{
		base.PrepareState();

		// Attach functions to view events
		owner.UI.LevelSelectionView.OnReturnClicked += ReturnClicked;
		owner.UI.LevelSelectionView.OnLevelClicked += LevelClicked;

		// Get number of unlocked levels from save data
		unlockedButtonCount = owner.PlayerController.gameObject.GetComponent<ProgressProcessor>().saveData.levelAchieved;

		// Enable buttons for all levels that have been completed by player
		for (int i = 0; i < unlockedButtonCount; i++)	
		{
			owner.UI.LevelSelectionView.Buttons[i].gameObject.SetActive(true);
		}

		// Set first button in list as current button and select it
		currentButtonNumber = 0;
		owner.UI.LevelSelectionView.selectButton(currentButtonNumber);

		// Show Level Selection view
		owner.UI.LevelSelectionView.ShowView();
	}

	public override void DestroyState()
	{
		// If destroying menu content is necessary, unload scene used in menu
		if (destroyMenuContent)
		{
			SceneManager.UnloadSceneAsync("Level3");
		}

		// Hide Level Selection view
		owner.UI.LevelSelectionView.HideView();

		// If state is being changed to Game, enable player's movement, sound and laser
		if (jumpToGame)
		{
			owner.PlayerController.EnableMovement();
			owner.PlayerController.EnableSound();
			owner.PlayerController.EnableLaser();
		}

		// Detach functions from view events
		owner.UI.LevelSelectionView.OnReturnClicked -= ReturnClicked;
		owner.UI.LevelSelectionView.OnLevelClicked -= LevelClicked;

		// Find event system object and remove its button/element selection
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(null);

		base.DestroyState();
	}

	public override void UpdateState()
	{
		base.UpdateState();

		// Rotate player analogically to Menu state
		owner.PlayerController.transform.Rotate(0, 10 * Time.deltaTime, 0);

		// Handle states of UI key presses
		HandleDownClicked();
		HandleUpClicked();
		HandleCancelClicked();
	}

	void HandleDownClicked()
	{
		// If Menu Control Down button is pressed
		if (Input.GetButtonDown("MenuControlDown"))
		{
			// If current button is the last unlocked level button, set first button in list as current button and select it
			if (currentButtonNumber == unlockedButtonCount - 1)
			{
				currentButtonNumber = 0;
				owner.UI.LevelSelectionView.selectButton(currentButtonNumber);
			}
			// Otherwise, set next button in list as current button and select it
			else
			{
				currentButtonNumber++;
				owner.UI.LevelSelectionView.selectButton(currentButtonNumber);
			}
		}
	}

	void HandleUpClicked()
	{
		// If Menu Control Up button is pressed
		if (Input.GetButtonDown("MenuControlUp"))
		{
			// If current button is the first button in list, set last unlocked level button as current button and select it
			if (currentButtonNumber == 0)
			{
				currentButtonNumber = unlockedButtonCount - 1;
				owner.UI.LevelSelectionView.selectButton(currentButtonNumber);
			}
			// Otherwise, set previous button in list as current button and select it
			else
			{
				currentButtonNumber--;
				owner.UI.LevelSelectionView.selectButton(currentButtonNumber);
			}
		}
	}

	void HandleCancelClicked()
	{
		// If Cancel button is pressed, invoke return function
		if (Input.GetButtonDown("Cancel"))
		{
			ReturnClicked();
		}
	}

	// Function called when one of the Level buttons is clicked in Level Selection view
	void LevelClicked(int level)
	{
		// Allow enabling player's movement and sound
		jumpToGame = true;

		// Set current level to level corresponding to clicked button
		owner.CurrentLevel = level;

		// Set current state to Loading Level state, starting with level based on value of CurrentLevel variable
		owner.ChangeState(new LoadingLevelState { levelToLoad = owner.CurrentLevel });
	}

	// Function called when Return button is clicked in Level Selection view
	void ReturnClicked()
	{
		// Prevent the game from unloading menu scene when state is changed
		destroyMenuContent = false;

		// Change state to Menu with preventing loading menu content again and signalising that the state was invoked from a submenu
		owner.ChangeState(new MenuState { loadMenuContent = false, jumpFromSubmenu = true });
	}
}
