using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Controls state, shows controls view
public class ControlsState : BaseState
{
	public override void PrepareState()
	{
		base.PrepareState();

		// Attach functions to view events
		owner.UI.ControlsView.OnReturnClicked += ReturnClicked;

		// Show Controls view
		owner.UI.ControlsView.ShowView();
	}

	public override void DestroyState()
	{
		// Hide Controls view
		owner.UI.ControlsView.HideView();

		// Detach functions from view events
		owner.UI.ControlsView.OnReturnClicked -= ReturnClicked;

		base.DestroyState();
	}

	public override void UpdateState()
	{
		base.UpdateState();

		// Rotate player analogically to Menu state
		owner.PlayerController.transform.Rotate(0, 10 * Time.deltaTime, 0);

		// Handle states of UI key presses
		HandleCancelClicked();
	}

	void ReturnClicked()
	{
		// Change state to Menu with preventing loading menu content again and signalising that the state was invoked from a submenu
		owner.ChangeState(new MenuState { loadMenuContent = false, jumpFromSubmenu = true });
	}

	void HandleCancelClicked()
	{
		// If Cancel button is pressed, invoke return function
		if (Input.GetButtonDown("Cancel"))
		{
			ReturnClicked();
		}
	}
}
