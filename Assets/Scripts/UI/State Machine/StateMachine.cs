using UnityEngine;
using UnityEngine.EventSystems;

// State Machine implementation, uses BaseState as base class for storing currently operating state
public class StateMachine : MonoBehaviour
{
    // Reference to currently operating state
    BaseState currentState;

    // Reference to UI root that holds references to different views
    [SerializeField] UIRoot ui;
    public UIRoot UI => ui;

    // Reference to player's controller component used to control its movement
    [SerializeField] PlayerController playerController;
    public PlayerController PlayerController => playerController;

    // Variable used for determining currently played level or level the game will start from
    public int CurrentLevel = 1;

    // Start is called before the first frame update
    void Start()
    {
        // Set state to Menu state
        ChangeState(new MenuState());
    }

    void Update()
    {
        // If current state is not null, update it
        if (currentState != null)
        {
            currentState.UpdateState();
        }
    }

    // Method used to change state
    public void ChangeState(BaseState newState)
    {
        // If current state is not null, destroy it
        if (currentState != null)
        {
            currentState.DestroyState();
        }

        // Set current state to state specified in newState variable
        currentState = newState;

        // If current state is not null (if new state is not a null state), assign state machine as its owner and initialise it
        if (currentState != null)
        {
            currentState.owner = this;
            currentState.PrepareState();
        }
    }

    // Method used to get current state
	public BaseState GetState()
    {
        return currentState;
    }
}