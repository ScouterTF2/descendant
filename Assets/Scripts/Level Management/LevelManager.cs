using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	// Reference to object which specifies player's start position
	[SerializeField] GameObject spawnLocation;
	public GameObject SpawnLocation => spawnLocation;

	// Reference to the area which ends the level
	[SerializeField] GameObject exitArea;
	public GameObject ExitArea => exitArea;

	// Reference to next level
	[SerializeField] int nextLevelNumber;
	public int NextLevelNumber => nextLevelNumber;

	// Variable to determine if player's laser should be enabled or disabled on start of level
	[SerializeField] bool laserEnabled;
	public bool LaserEnabled => laserEnabled; 
}
