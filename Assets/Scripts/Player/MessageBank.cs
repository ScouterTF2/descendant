using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MessageBank
{
	// Dictionary containing all possible messages and their identificators
	public static Dictionary<string, List<string>> Messages = new Dictionary<string, List<string>>()
	{
		{ "Message1", new List<string> {
				"Dzień 20453, 09:40 M. Wilson [SYSTEM]: #0341 i #0752 | wyjście | komentarz: \"Nie, dzięki, nie potrzebujemy asysty.\"",
				
				"Dzień 20453, 09:42 M. Wilson -> M. Wilson: ughhhh głupie wymagania\n"
				+ "Dzień 20453, 09:42 M. Wilson -> M. Wilson: uśmiechnij się trochę Mrge, zawsze asystuj Marge\n"
				+ "Dzień 20453, 09:43 M. Wilson -> M. Wilson: to nie jest mój dzień, ok?",
				
				"Dzień 20453, 10:01 M. Wilson -> M. Wilson: nuuudaaaaa.",
				
				"Dzień 20453, 11:33 M. Wilson [SYSTEM]: #0341 i #0752 | wejście | komentarz: \"Jak zwykle, żadnych anomalii, szczegóły w dystrykcie 4.\"\n"
				+ "Dzień 20453, 11:35 M. Wilson -> M. Wilson: czy naprawdę muszę to za każdym razem pisać, skoro i tak nigdy nie ma żadnych anomalii?",
				
				"Dzień 20453, 12.26 M. Wilson -> M. Wilson: .",
				
				"Dzień 20453, 12.28 M. Wilson -> M. Wilson: ..",
				
				"Dzień 20453, 12.30 M. Wilson -> M. Wilson: …",
				
				"Dzień 20453, 12.32 M. Wilson -> M. Wilson: ….",
				
				"Dzień 20453, 12.34 M. Wilson -> M. Wilson: yaay! 12:34!",
				
				"Dzień 20453, 14:55 M. Wilson [SYSTEM]: #0301 | wyjście | komentarz: \"Tylko na chwilę wyjdę, ok? Tylko chcę coś sprawdzić.\"\n"
				+ "Dzień 20453, 14:56 M. Wilson -> M. Wilson: ?",
				
				"Dzień 20453, 15:01 M. Wilson [SYSTEM]: #0301 | wejście | komentarz: \"Tak, dzięki, już nic takiego haha\"\n"
				+ "Dzień 20453, 15.01 M. Wilson -> M. Wilson: ????\n"
				+ "Dzień 20453, 15.01 M. Wilson: > log last report person-id to check\n"
				+ "Dzień 20453, 15.01 M. Wilson [SYSTEM]: Error: Nieprawidłowa opcja \"to\".",
				
				"Dzień 20453, 15.03 M. Wilson -> M. Wilson: aaAAAAaaaa",
				
				"Dzień 20453, 15.03 M. Wilson: > log last report person-id to-check\n"
				+ "Dzień 20453, 15.04 M. Wilson [SYSTEM]: Oznaczono \"#0301\" jako \"do sprawdzenia\". Dziękujemy za Twoją czujność.",
				
				"Dzień 20453, 15.04 M. Wilson -> M. Wilson: śmierć komputerom.",
				
				"Dzień 20453, 17:08 M. Wilson -> M. Wilson: może zacznę pisać wiersz?\n"
				+ "Dzień 20453, 17:20 M. Wilson -> M. Wilson: W kosmcznej pustce / Pod skałą ponurą\n"
				+ "Dzień 20453, 17:20 M. Wilson -> M. Wilson: nie",
				
				"Dzień 20453, 17:23 M. Wilson -> M. Wilson: W kosmicznej pustce / W skalistej ciemności\n"
				+ "Dzień 20453, 17:24 M. Wilson -> M. Wilson: Gdzie bielą się kości / Za wielkim głazem\n"
				+ "Dzień 20453, 17:24 M. Wilson -> M. Wilson: Pod stalowym włazem / Szukam mądrości.\n"
				+ "Dzień 20453, 17:25 M. Wilson -> M. Wilson: bardzo głębokir. mistrzyni poezji. geniusz na miarę swojego wieku.",
				
				"Dzień 20453, 20:20 M. Wilson -> M. Wilson: ehhhh kolejny niesamowicie produktywny dzień",
				
				"Dzień 20454, 00:00 ??? [SYSTEM]: Error: Brak dalszych logów. Zgłoszono niekompletny zapis."
			}
		},
		{ "Message2", new List<string> {
				"[20449, 20:00] od Kitka: Hejj Marge~ jak tam pierwszy tydzień w nowej pracy? może wpadnę jutro na chwilę, co? ",
				
				"[20449, 20:32] do Kitka: Tak proszę błagam wszystko byle nie ta nuda",
				
				"[20449, 20:33] od Kitka: Haha, aż tak źle.?",
				
				"[20449, 20:33] do Kitka: Nic. się. nie. dzieje.\n"
				+ "[20449, 20:33] do Kitka: Przez cały dzień.\n"
				+ "[20449, 20:33] do Kitka: Może i to lepsze po tym, co się stało. Ale serio. nie wiem jak długo to zniosę",
				
				"[20449, 20:34] od Kitka: ok, ok, przyjdę jutro rano, mam wolne. \n"
				+ "[20449, 20:35] od Kitka: Wszystko się ułoży laska. Pogadamy I będzie lepiej. Zobaczysz~!",
				
				"[20449, 20:35] do Kitka: mhm.",
				
				"[20449, 20:36] od Kitka: Przyniosę ciastka z Ziemi.",
				
				"[20449, 20:36] do Kitka: …przynieś.",
				
				"[20452, 21:12] do Kitka: Hej. Przyjdziesz jutro?",
				
				"[20452, 22:45] od Kitka: Sorry, nic z tego. Wszyscy są trochę napięci. \n"
				+ "[20452, 22:45] od Kitka: Wiesz, bardziej niż zazwyczaj haha\n"
				+ "[20452, 22:46] od Kitka: Ale postaram się wpaść następnym razem. obiecuję.!",
				
				"[20452, 22:52] od Kitka: hej, trzymaj się tam!",
				
				"[20453, 21:01] do Kitka: hej. możemy jutro pogadać?",
				
				"[20453, 22:36] do Kitka: naprawdę tego potrzebuję.",
				
				"[20454, 07:21] do Kitka: Kitka?",
				
				"[20454, 20:30] do Kitka: jesteś? nie mogę się z tobą skontaktować",
				
				"[20455, 07:17] do Kitka: Kitka???",
				
				"[20455, 14:24] > msg-delete all self\n"
				+ "[20455, 14:24] [SYSTEM]: Usunięto wszystkie prywatne notatki.\n"
				+ "[20455, 14:24] > disconnect\n"
				+ "[20455, 14:24] [SYSTEM]: Odłączono od sieci. Uwaga: odłączenie prywatnego terminala od sieci może skutkować brakiem dostępu do ważnych komunikatów.",
				
				"[20455, 14:25]: coś jest nie tak. czy goście z it NA PEWNO nie mają wglądu w moje notatki?\n"
				+ "[20455, 14:25]: mogłabym przysiąc, że nigdzie indziej tego nie pisałam.\n"
				+ "[20455, 14:26]: od teraz zero notatek w terminalu z recepcji.",
				
				"[20455, 14:26]: i co z tego, że nie chcę mieć tych szkieł? to moja prywatna sprawa.\n"
				+ "[20455, 14:26]: poza tym wiele innych osób też ich nie znosi.",
				
				"[20455, 14:27]: i Kitka gdzieś zniknęła. i parę innych osób, z tego co wiem.",
				
				"[20455, 14:27]: to nie ma sensu. niby gdzie by się podziali? to jest jedyne wyjście ze stacji.",
				
				"[20455, 15:57]: kolejne 2 osoby zostały zgłoszone jako zaginione…\n"
				+ "[20455, 15:58]: to jest chore.",
				
				"[20455, 16:04]: …ktoś tu leci. muszę wracać na miejsce.\n"
				+ "[20455, 16:04] > msg delete all self\n"
				+ "[20455, 16:04] [SYSTEM]: Error: Nieprawidłowa komenda \"msg\"."
			}
		},
		{ "l3m1_info", new List<string> {
				"Dzień 20444, 14:52 J. Larsson: Panel konserwacyjny w 301 znowu nieszczelny, zajmijcie się tym",
				
				"Dzień 20444, 14:52 J. Larsson: przesyłam dane\n"
				+ "Dzień 20444, 14:52 J. Larsson -> P. Kowalski [SYSTEM]: Obiekt AG301#195 wymaga natychmiastowej uwagi. Zaktualizowano położenie obiektu na mapie.\n"
				+ "Dzień 20444, 14:52 J. Larsson -> A. Moreau [SYSTEM]: Obiekt AG301#195 wymaga natychmiastowej uwagi. Zaktualizowano położenie obiektu na mapie.",
				
				"Dzień 20444, 14:53 J. Larsson: Paul, uprzedzam, dostęp wymaga szkieł. nadal masz problem z używaniem?",
				
				"Dzień 20444, 14:53 P. Kowalski: już nie idę",
				
				"Dzień 20444, 14:54 A. Moreau: Interpunkcja! Używaj przecinków, inaczej nie da się zrozumieć, co piszesz. \n"
				+ "Dzień 20444, 14:55 A. Moreau: Twoja wypowiedź może znaczyć dwie różne rzeczy. Komunikacja to podstawa zrozumienia w pracy.",
				
				"Dzień 20444, 14:56 P. Kowalski: podstawa zrozumienia to nie bycie dupkiem arthur\n"
				+ "Dzień 20444, 14:56 P. Kowalski [SYSTEM]: Ostrzeżenie (2): Zachowaj odpowiednie słownictwo.",
				
				"Dzień 20444, 14:57 J. Larsson: Panowie, proszę. To publiczna sieć.\n"
				+ "Dzień 20444, 14:57 J. Larsson: możecie dokończyć dyskusję później osobiście, na razie proszę do pracy.\n"
				+ "Dzień 20444, 14:57 P. Kowalski: już w drodze\n"
				+ "Dzień 20444, 14:58 A. Moreau: Idę, już idę.",
				
				"Dzień 20444, 15:14 P. Kowalski -> J. Larsson: wystarczy popatrzeć przez to cacko tak?",
				
				"Dzień 20444, 15:14 J. Larsson -> P. Kowalski: Popatrzeć i strzelić żebyś mógł przejść.",
				
				"Dzień 20444, 15:15 P. Kowalski -> J. Larsson: przypomnij mi\n"
				+ "Dzień 20444, 15:15 P. Kowalski -> J. Larsson: po co nam to jest",
				
				"Dzień 20444, 15:16 J. Larsson -> P. Kowalski: żeby taka recepcjonistka na przykład nie chodziłą sobie tunelami konserwacyjnymi bez powodu. autoryzacja, Paul.",
				
				"Dzień 20444, 15:17 J. Larsson -> P. Kowalski: bez odpowiedniego koloru szkła nie przejdziesz.",
				
				"Dzień 20444, 15:19 J. Larsson -> P. Kowalski: W razie problemów możesz poprosić Arthura o pomoc\n"
				+ "Dzień 20444, 15:19 P. Kowalski -> J. Larsson: nigdy. ",
				
				"Dzień 20445, 00:00 ??? [SYSTEM]: Error: Brak dalszych logów. Zgłoszono niekompletny zapis."
			}
		},
		{ "l3m2_glass", new List<string> {
				"Dzień 20440, 06:41 do Erudyta: co to ma być\n"
				+ "Dzień 20440, 06:41 > contact name Erudyta set dupek\n"
				+ "Dzień 20440, 06:41 [SYSTEM]: Zmieniono nazwę Erudyta na dupek.\n"
				+ "Dzień 20440, 06:42 do dupek: kiedy się dostałeś do mojego notatnika",
				
				"Dzień 20440, 06:48 od dupek: Nie wiem, o czym mówisz.",
				
				"Dzień 20440, 06:49 do dupek: grzebałeś mi w osobistych notatkach?",
				
				"Dzień 20440, 06:49 od dupek: Schowałeś moje narzędzia, kiedy ich potrzebowałem?",
				
				"Dzień 20440, 06:50 do dupek: to co innego. to moja osobista własność. zawieszą cię",
				
				"Dzień 20440, 06:51 od dupek: Pierwsze słyszę. Poza tym nie masz żadnych dowodów.",
				
				"Dzień 20440, 22:10 od AnnaIngram: Paul, mam prośbę\n"
				+ "Dzień 20440, 22:11 od AnnaIngram: znowu mi się hydrokomora zepsuła…",
				
				"Dzień 20440, 22:13 od AnnaIngram: Wiem, że jest późno, ale córka musi umyć na jutro głowę.",
				
				"Dzień 20440, 22:17 od AnnaIngram: pomożesz ?",
				
				"Dzień 20440, 22:22 do AnnaIngram: nie ma problemu anna\n"
				+ "Dzień 20440, 22:22 do AnnaIngram: będę za 10 min",
				
				"Dzień 20440, 22:24 od AnnaIngram: nie wiem jak Ci dziękować!!! nie martw się, zdejmę szkła na Twoje przyjście!",
				
				"Dzień 20442, 20:02 do Jakob: jakob\n"
				+ "Dzień 20442, 20:02 do Jakob: czemu to cholerstwo jest teraz w tunelach?",
				
				"Dzień 20442, 20:05 do Jakob: nie można się od tego odpędzic przechodząc przez stacje\n"
				+ "Dzień 20442, 20:05 do Jakob: i tutaj też mam to znosić?",
				
				"Dzień 20442, 20:42 od Jakob: To zabezpieczenie. Żeby taka recepcjonistka na przykład nie chodziłą sobie tunelami konserwacyjnymi bez powodu",
				
				"Dzień 20442, 20:45 do Jakob: co było nie tak z kodami?",
				
				"Dzień 20442, 20:46 od Jakob: Ktoś je przechwycił. To jest po prostu wygodniejsze rozwiązanie. po więcej informacji możesz napisać do kierownictwa.\n"
				+ "Dzień 20442, 20:47 od Jakob: nie martw się, to proste, szybko się nauczysz\n"
				+ "Dzień 20442, 20:47 do Jakob: i przydatne\n"
				+ "Dzień 20442, 20:48 od Jakob: Tobie też polecam – mógłbyś na przykład trzyamć za własnym szkłem osobisty notatnik.",
				
				"Dzień 20442, 20:49 do Jakob: nie ma mowy, nie skorzystam\n"
				+ "Dzień 20442, 20:49 do Jakob: i tak nie mam nic do ukrycia",
				
				"Dzień 20442, 20:50 od Jakob: jak wolisz. ale proszę, następnym razem nie zgłaszaj się do mnie, jeśli ktoś ci tam coś poprzestawia. ",
				
				"Dzień 20442, 20:50 do Jakob: to był arthur.",
				
				"Dzień 20442, 20:50 od Jakob: Nie mam na to dowodów, Paul. "
			}
		},
		{ "l3m3_progress", new List<string> {
				"Dzień 20438, 20:00 od x?z: Kiedyś, by zorientować się, kto stoi ponad Tobą, wystarczyło spojrzeć w górę. Teraz nie ma pojęcia takiego jak góra czy dół. Każdy kierunek, który obierzesz, jest tym, który będzie przed Tobą.\n"
				+ "Dzień 20438, 20:00 od xyz: Czas Twojego zadania się zbliża. ",

				"Dzień 20439, 08:00 od xyz: Nawet najdrobniejsze szczegóły wpływają na nasze postrzeganie otoczenia.\n"
				+ "Dzień 20439, 08:00 do ?yz: Jeśli nauczysz się wykorzystywać to, co możesz zmienić, wkrótce okaże się, że możesz zmienić wszystko. Musisz uczynić ten niewielki krok naprzód.",
				
				"Dzień 20439, 08:07 do xyz: A gdzie jest, Twoim zdaniem, granica? Jak wiele można poświęcić w poszukiwaniu prawdy dla samego siebie?\n"
				+ "Dzień 20439, 08:08 od ?y?: Gdybyśmy nie przesuwali granic, nie byłoby nas teraz poza Układem Słonecznym. Twoje zadanie to bariera nie do pokonania tylko dla ograniczonego umysłu.",

				"Dzień 20439, 20:00 od x??: Miarą progresu jest zmiana, a postęp technologiczny służy jako narzędzie dla umysłu.\n"
				+ "Dzień 20439, 20:00 od xyz: Twoje słowa.\n"
				+ "Dzień 20439, 20:01 od x?z: Czekam do jutra.",

				"Dzień 20440, 06:41 od Frajer: co to ma być\n"
				+ "Dzień 20440, 06:42 od Frajer: kiedy się dostałeś do mojego notatnika",
				
				"Dzień 20440, 06:48 do Frajer: Nie wiem, o czym mówisz.",
				
				"Dzień 20440, 06:49 od Frajer: grzebałeś mi w osobistych notatkach?",
				
				"Dzień 20440, 06:49 do Frajer: Schowałeś moje narzędzia, kiedy ich potrzebowałem?",
				
				"Dzień 20440, 06:50 od Frajer: to co innego. to moja osobista własność. zawieszą cię",
				
				"Dzień 20440, 06:51 do Frajer: Pierwsze słyszę. Poza tym nie masz żadnych dowodów.\n",
				
				"Dzień 20440, 06:54 [SYSTEM]: PILNE: Możliwe wykroczenie w miejscu pracy. Zgłoś się do Jakoba Larssona.\n"
				+ "Dzień 20440, 06:54: …co ja robię?",

				"Dzień 20440, 08:00 od x?z: Podjąłeś decyzję?\n"
				+ "Dzień 20440, 08:10 od x??: Arthur.\n"
				+ "Dzień 20440, 08:13 do xyz: …Zrobiłem, co chciałeś. Odezwę się wieczorem. ",

				"Dzień 20440, 20:00 od ?yz: Nie pozwól, by zmartwienia wzięły nad Tobą kontrolę. Ta niewielka zmiana już niedługo oczyści Twój umysł z wątpliwości.\n"
				+ "Dzień 20440, 20:04 do xyz: Wiesz, chyba muszę odpocząć przez kilka dni.\n"
				+ "Dzień 20440, 20:05 > contact block xyz all\n"
				+ "Dzień 20440, 20:05 [SYSTEM]: Ostrzeżenie: Blokada spowoduje brak informacji o przychodzących wiadomościach. Kontynuować? [y/n]",
				
				"Dzień 20440, 20:05 od ???: Nie rób tego. Ostrzegam.",
				
				"Dzień 20440, 20:05 > y\n"
				+ "Dzień 20440, 20:05 [SYSTEM]: Zablokowano xyz.",

				"##@Dzie? 2?440, ??:?? [SY?TEM]: @rror: Nie?omple?ne logi?#",

				"Dzień 20444, 11:12 do Paul: Musimy porozmawiać.",
				
				"Dzień 20444, 16:42 do Paul: Miałem… problemy, ale już wszystko się ułożyło. Chyba.",
				
				"Dzień 20445, 15:42 do Paul: Jeśli nie chcesz rozmawiać, przyjmij chociaż przeprosiny. Przepraszam.\n"
				+ "Dzień 20445, 15:48 do Paul: Jeśli będziesz potrzebował pomocy, na przykład ze szkłami… wiesz kogo spytać.",
				
				"Dzień 20445, 16:14 od Paul: ok. porozmawiamy jutro ale w cztery oczy",
				
				"Dzień 20446, 20:00 do Paul: Wiesz, w tym ostatnim zdaniu zabrakło Ci przecinka.\n"
				+ "Dzień 20446, 20:03 od Paul: dupek. twoja metrówka jutro magicznie znajdzie sie w innym miejscu\n"
				+ "Dzień 20446, 20:05 do Paul: Frajer."
			}
		},
		{ "l3m4_family", new List<string> {
				"abc\n"
				+ "def",
				"abc"
			}
		}
	};

}
