using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControlsView : BaseView
{
	// Events to attach to
	public UnityAction OnReturnClicked;

	// Method called by Return Button
	public void ReturnClick()
	{
		OnReturnClicked?.Invoke();
	}
}
