using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIToggle : UIInputElement
{
	// Reference to toggle object
	[SerializeField] public Toggle toggle;
}
