using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

// Game state, shows game view and loads content related to gameplay
public class GameState : BaseState
{
    // Variable used for destroying game content
    public bool destroyGameContent = true;

    public override void PrepareState()
    {
        base.PrepareState();

        // Show Game view
        owner.UI.GameView.ShowView();

        // Attach functions to view events
        owner.UI.GameView.OnPauseClicked += PauseClicked;
        owner.UI.GameView.OnRespawnClicked += RespawnClicked;
    }

    public override void DestroyState()
    {
        // Detach functions from view events
        owner.UI.GameView.OnPauseClicked -= PauseClicked;
        owner.UI.GameView.OnRespawnClicked -= RespawnClicked;

        // If destroying game content is necessary, drop item from player's item arm if there is one in it and unload specified level
        if (destroyGameContent)
        {
            if (owner.PlayerController.ItemPickup.Item != null)
            {
                owner.PlayerController.ItemPickup.DropItem();
            } 

            SceneManager.UnloadSceneAsync("Level" + owner.CurrentLevel);
        }

        // Hide Game view
        owner.UI.GameView.HideView();

        base.DestroyState();
    }

    public override void UpdateState()
    {
        base.UpdateState();

        // Handle states of UI key presses
		HandlePauseClicked();
        HandleRespawnClicked();
    }

    // Function called when Pause button is clicked in Game view
    void PauseClicked()
    {
        // Prevent the game from unloading current level when state is changed
        destroyGameContent = false;

        // Set current state to Pause state
        owner.ChangeState(new PauseState());
    }

    // Function called when Respawn button is clicked in Game view
    void RespawnClicked()
    {
        // Set current state to Loading Level state, starting with level based on value of CurrentLevel variable and with disabling player's movement, laser and sound in the state
		owner.ChangeState(new LoadingLevelState { levelToLoad = owner.CurrentLevel, jumpFromGame = true });
    }

	void HandlePauseClicked()
	{
		// If Cancel button is pressed, invoke pause function
		if (Input.GetButtonDown("Cancel"))
		{
			PauseClicked();
		}
	}

    void HandleRespawnClicked()
    {
        // If Jump button is pressed, invoke respawn function
		if (Input.GetButtonDown("Jump"))
		{
			RespawnClicked();
		}
    }
}