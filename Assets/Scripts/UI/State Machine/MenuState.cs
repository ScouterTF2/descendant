using System;
using UnityEngine;
using UnityEngine.SceneManagement;

// Menu state, shows Menu view and adds user interaction with that view
public class MenuState : BaseState
{
    // Variables used for loading and destroying menu content
    public bool loadMenuContent = true;
    public bool destroyMenuContent = true;

    // Variable used for disabling player's movement and sound when NOT changing state from a submenu
    public bool jumpFromSubmenu = false;

	// Reference to number of currently selected button from buttons list
	int currentButtonNumber;

	public override void PrepareState()
    {
        base.PrepareState();

        // Attach functions to view events
        owner.UI.MenuView.OnStartClicked += StartClicked;
		owner.UI.MenuView.OnContinueClicked += ContinueClicked;
		owner.UI.MenuView.OnLevelsClicked += LevelsClicked;
		owner.UI.MenuView.OnControlsClicked += ControlsClicked;
        owner.UI.MenuView.OnOptionsClicked += OptionsClicked;
        owner.UI.MenuView.OnQuitClicked += QuitClicked;

		// If submenu was not a previous state, disable player's movement, sound and laser and reset his velocity
		if (!jumpFromSubmenu)
        {
            owner.PlayerController.DisableMovement();
            owner.PlayerController.DisableSound();
            owner.PlayerController.DisableLaser();
			owner.PlayerController.ResetVelocity();
        }

		// Set first button in list as current button and select it
		currentButtonNumber = 0;
		owner.UI.MenuView.selectButton(currentButtonNumber);

		// Show Menu view
		owner.UI.MenuView.ShowView();

        // If loading menu content is necessary, load Level 3 scene and set player's position and rotation to center of scene
        if (loadMenuContent)
        {
            SceneManager.LoadScene("Level3", LoadSceneMode.Additive);
            owner.PlayerController.transform.position = new Vector3(8.66f, -1.47f, -32.22f);
            owner.PlayerController.transform.rotation = new Quaternion(0, 0, 0, 1);
        }
    }

	public override void DestroyState()
    {
        // If destroying menu content is necessary, unload scene used in menu
        if (destroyMenuContent)
        {
            SceneManager.UnloadSceneAsync("Level3");
        }

        // Hide Menu view
        owner.UI.MenuView.HideView();

        // Detach functions from view events
        owner.UI.MenuView.OnStartClicked -= StartClicked;
		owner.UI.MenuView.OnContinueClicked -= ContinueClicked;
		owner.UI.MenuView.OnLevelsClicked -= LevelsClicked;
		owner.UI.MenuView.OnControlsClicked -= ControlsClicked;
        owner.UI.MenuView.OnOptionsClicked -= OptionsClicked;
        owner.UI.MenuView.OnQuitClicked -= QuitClicked;

		// Find event system object and remove its button/element selection
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(null);

		base.DestroyState();
    }

    public override void UpdateState()
    {
        base.UpdateState();

        // Rotate player so that he rotates with a certain speed around Y axis, independent from framerate
        owner.PlayerController.transform.Rotate(0, 10 * Time.deltaTime, 0);

		// Handle states of UI key presses
		HandleDownClicked();
		HandleUpClicked();
	}

	void HandleDownClicked()
	{
		// If Menu Control Down button is pressed
		if (Input.GetButtonDown("MenuControlDown"))
		{
			// If current button is the last button in list, set first button in list as current button and select it
			if (currentButtonNumber == owner.UI.MenuView.Buttons.Count - 1)
			{
				currentButtonNumber = 0;
				owner.UI.MenuView.selectButton(currentButtonNumber);
			}
			// Otherwise, set next button in list as current button and select it
			else
			{
				currentButtonNumber++;
				owner.UI.MenuView.selectButton(currentButtonNumber);
			}
		}
	}

	void HandleUpClicked()
	{
		// If Menu Control Up button is pressed
		if (Input.GetButtonDown("MenuControlUp"))
		{
			// If current button is the first button in list, set last button in list as current button and select it
			if (currentButtonNumber == 0)
			{
				currentButtonNumber = owner.UI.MenuView.Buttons.Count - 1;
				owner.UI.MenuView.selectButton(currentButtonNumber);
			}
			// Otherwise, set previous button in list as current button and select it
			else
			{
				currentButtonNumber--;
				owner.UI.MenuView.selectButton(currentButtonNumber);
			}
		}
	}
    
    // Function called when Start button is clicked in Menu view
    void StartClicked()
    {
		// Reset current level to level 1
        owner.CurrentLevel = 1;

        // Set current state to Loading Level state, starting with level based on value of CurrentLevel variable
        owner.ChangeState(new LoadingLevelState { levelToLoad = owner.CurrentLevel });
        
    }

	// Function called when Continue button is clicked in Menu view
	void ContinueClicked()
	{
        // Set current level to level set in save data
		owner.CurrentLevel = owner.PlayerController.GetComponent<ProgressProcessor>().saveData.currentLevel;

		// Set current state to Loading Level state, starting with level based on value of CurrentLevel variable
		owner.ChangeState(new LoadingLevelState { levelToLoad = owner.CurrentLevel });
	}

	// Function called when Levels button is clicked in Menu view
	void LevelsClicked()
	{
		// Prevent the game from unloading menu scene when state is changed
		destroyMenuContent = false;

        // Set current state to Level Selection state
		owner.ChangeState(new LevelSelectionState());
	}

	void ControlsClicked()
	{
		// Prevent the game from unloading menu scene when state is changed
		destroyMenuContent = false;

		// Set current state to Controls state
		owner.ChangeState(new ControlsState());
	}

	// Function called when Options button is clicked in Menu view
	void OptionsClicked()
    {
        // Prevent the game from unloading menu scene when state is changed
        destroyMenuContent = false;

        // Set current state to Options state
        owner.ChangeState(new OptionsState { returnToState = OptionsState.ReturnToStates.Menu });
    }

    // Function called when Quit button is clicked in Menu view
    void QuitClicked()
    {
        // Quit game
        Application.Quit();
    }
}