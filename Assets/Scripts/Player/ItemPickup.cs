using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ItemPickup : MonoBehaviour
{
	// Reference to player's controller
	[SerializeField] PlayerController playerController;

	// Reference to item object used to return it to normal state
    Transform item;
	public Transform Item => item;

    // Variable used to store parent of item object for putting it back in the same place after dropping it
    Transform parent;

	// Audio component
	AudioSource audioSource;

	// Reference to audio files for picking and dropping items
	[SerializeField] AudioClip pickSound;
	[SerializeField] AudioClip dropSound;

	// Main module ofarticle system component from laser object
    ParticleSystem.MainModule laserMain;

    // Dictionary that stores all glass tags and their respective colors
    Dictionary<string, Color32> glassColors;

    // Reference to UI glass view object
    [SerializeField] Image glassView;

	// Reference to state machine
	[SerializeField] StateMachine stateMachine;

	// Variable to determine if currently picked object is a glass object
	bool? isGlass;

	// Start is called before the first frame update
	void Start()
    {
		// Get audio source component
		audioSource = GetComponent<AudioSource>();

		// Get main module of laser's particle system
        laserMain = FindObjectOfType<ParticleSystem>().main;

        // Initialise colors dictionary with tags used for glasses and color values used to change laser start color
        glassColors = new Dictionary<string, Color32>();
        glassColors.Add("Red Glass", Color.red);
        glassColors.Add("Green Glass", Color.green);
        glassColors.Add("Blue Glass", Color.blue);
        glassColors.Add("Default Glass", Color.white);
	}

    // Update is called once per frame
    void Update()
    {
		// If Action button is pressed
		if (Input.GetButtonDown("Action"))
		{
			// If glass object is currently picked, drop it
            if (isGlass == true)
            {
                DropItem();
            }
			else
			{
				// Create a ray based on player's camera position - it will be used to allow picking items only in a certain distance from it
				var ray = playerController.MainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

				// Cast a ray with max distance of 1.5 from the player and check if no errors happened
				if (Physics.Raycast(ray, out RaycastHit hit, 1.5f))
				{
					// If object in ray's distance has a Rigidbody
					if (hit.transform.GetComponent<Rigidbody>() != null)
					{
						// If object in ray's distance is a glass object
						if (glassColors.ContainsKey(hit.transform.tag))
						{
							// Set isGlass state variable to true
							isGlass = true;

							// Pick glass object
							PickItem(hit.transform.GetComponent<Transform>());
						}
						// If object in ray's distance is a floppy object
						else if (hit.transform.tag.Contains("Floppy"))
						{
							// Set isGlass state variable to false
							isGlass = false;

							// Pick floppy object
							PickItem(hit.transform.GetComponent<Transform>());
						}
					}
				}
			}
		}
	}

	void PickItem(Transform pickable)
	{
		// Save item object in variable
        item = pickable;

		// If item object has a Rigidbody, set it as kinematic and set its velocity to zero
		if (pickable.GetComponent<Rigidbody>() != null)
		{
			// Set item object as kinematic and set its velocity to zero
        	pickable.GetComponent<Rigidbody>().isKinematic = true;
        	pickable.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        	pickable.GetComponent<Rigidbody>().velocity = Vector3.zero;
		}

        // Store reference to current item parent in helper variable
        parent = item.transform.parent;

        // Move glass object to item arm object
        pickable.transform.SetParent(gameObject.transform);

        // Reset relative position and rotation of item object
        pickable.transform.localPosition = Vector3.zero;
        pickable.transform.localEulerAngles = Vector3.zero;

		// Disable item object
		pickable.gameObject.SetActive(false);

		// Stop any sounds playing from audio source
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        
        // If sound is enabled, play pick sound
        if (playerController.Sound)
        {
            audioSource.PlayOneShot(pickSound);
        }	

		// If glass was picked
        if (isGlass == true)
        {
            // Set laser's start color to that color
            laserMain.startColor = new ParticleSystem.MinMaxGradient(glassColors[pickable.transform.tag]);

            // Enable UI glass view and corresponding glass view camera
            glassView.gameObject.SetActive(true);
            playerController.GlassViewCamera.gameObject.SetActive(true);

            // Enable shader quad and apply a material corresponding to picked glass color on it
            playerController.ShaderQuad.gameObject.SetActive(true);
            playerController.ShaderQuad.material = Resources.Load("Materials/" + pickable.transform.tag + " Shader", typeof(Material)) as Material;
        }
		// If item was picked
		else if (isGlass == false)
		{
			// Get current state (should be always Game state) and prevent it from destroying its content
			var tmp = (GameState)stateMachine.GetState();
			tmp.destroyGameContent = false;
	
			// Set current state to Message state with message text from picked item
			stateMachine.ChangeState(new MessageState { messageText = MessageBank.Messages[pickable.gameObject.GetComponent<MessageKeyHolder>().keyName] });
		}
	}

	public void DropItem()
	{
		// Enable item object
		item.gameObject.SetActive(true);

		// If item object has a Rigidbody, set it as not kinematic and set its velocity to identical as player's velocity
		if (item.GetComponent<Rigidbody>() != null)
		{
			// Set item object as kinematic and set its velocity to zero
        	item.GetComponent<Rigidbody>().isKinematic = false;
        	item.GetComponent<Rigidbody>().velocity = stateMachine.PlayerController.RigidBody.velocity;
		}

		// Restore previous position of item in hierarchy and set parent reference to null
        item.transform.SetParent(parent);
        parent = null;

        // Remove object reference and move item object back to root (it will be put in camera's coordinates)
        item = null;

		// Stop any sounds playing from audio source
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        
        // If sound is enabled, play drop sound
        if (playerController.Sound)
        {
            audioSource.PlayOneShot(dropSound);
        }

		// If glass was picked
		if (isGlass == true)
		{
			// Set laser's start color sprite back to default
			laserMain.startColor = new ParticleSystem.MinMaxGradient(Color.white);

			// Disable UI glass view and glass view camera
			glassView.gameObject.SetActive(false);
			playerController.GlassViewCamera.gameObject.SetActive(false);

			// Disable shader quad
			playerController.ShaderQuad.gameObject.SetActive(false);
		}

		// Clear glass state variable
		isGlass = null;
	}
}
