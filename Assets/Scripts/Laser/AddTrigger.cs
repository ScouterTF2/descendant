using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddTrigger : MonoBehaviour
{
    // Particle system component from laser object
    ParticleSystem laser;

    // Start is called before the first frame update
    void Start()
    {
        // Find laser object in scene
        laser = FindObjectOfType<ParticleSystem>();

        // If laser object is added to scene, add object to list of laser triggers
        if (laser)
        {
            laser.trigger.AddCollider(gameObject.transform);
        }
    }

    // OnDestroy is called when the object is removed from scene or if the scene itself is removed
    void OnDestroy()
    {
        // If laser object is added to scene, remove object from list of laser triggers
        if (laser)
        {
            laser.trigger.RemoveCollider(gameObject.transform);
        }
    }
}
