using UnityEngine;
using UnityEngine.Events;

// Game view class, passes button events
public class GameView : BaseView
{
    // Events to attach to
    public UnityAction OnPauseClicked;
    public UnityAction OnRespawnClicked;

    // Method called by Pause button
    public void PauseClick()
    {
        // Invoke action set to Pause button
        OnPauseClicked?.Invoke();
    }

    // Method called by Respawn button
    public void RespawnClick()
    {
        // Invoke action set to Respawn button
        OnRespawnClicked?.Invoke();
    }
}
