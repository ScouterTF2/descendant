using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

// Options view class, passes button events
public class OptionsView : BaseView
{
    // Events to attach to
    public UnityAction OnReturnClicked;
    public UnityAction OnMusicVolumeChanged;
    public UnityAction OnEffectsVolumeChanged;
    public UnityAction OnFOVChanged;
    public UnityAction OnPreviousResolutionClicked;
    public UnityAction OnNextResolutionClicked;
    public UnityAction OnVSyncChanged;

    // References to volume sliders
    [SerializeField] Slider musicVolumeSlider;
    public Slider MusicVolumeSlider => musicVolumeSlider;
    [SerializeField] Slider effectsVolumeSlider;
    public Slider EffectsVolumeSlider => effectsVolumeSlider;

    // Reference to Field Of View slider
    [SerializeField] Slider fovSlider;
    public Slider FOVSlider => fovSlider;

    // Reference to resolution text object
    [SerializeField] TMP_Text resolutionValue;
    public TMP_Text ResolutionValue => resolutionValue;

    // Reference to VSync toggle
    [SerializeField] Toggle vSyncToggle;
    public Toggle VSyncToggle => vSyncToggle;

    // Reference to Music Volume value text
    [SerializeField] TMP_Text musicVolumeValueText;
    public TMP_Text MusicVolumeValueText => musicVolumeValueText;

    // Reference to Effects Volume value text
    [SerializeField] TMP_Text effectsVolumeValueText;
    public TMP_Text EffectsVolumeValueText => effectsVolumeValueText;

    // Reference to Field Of View value text
    [SerializeField] TMP_Text fovValueText;
    public TMP_Text FOVValueText => fovValueText;

	// List of selectable elements for keyboard/gamepad functionality
	[SerializeField] List<UIInputElement> elements;
    public List<UIInputElement> Elements => elements;

	// Reference to object of currently selected element
	UIInputElement currentlySelectedElement;
    public UIInputElement CurrentlySelectedElement => currentlySelectedElement;

	// Method called by Return Button
	public void ReturnClick()
    {
        OnReturnClicked?.Invoke();
    }

    // Method called by Music Volume slider
    public void ChangeMusicVolume()
    { 
        OnMusicVolumeChanged?.Invoke(); 
    }

    // Method called by Effects Volume slider
    public void ChangeEffectsVolume()
    { 
        OnEffectsVolumeChanged?.Invoke(); 
    }

    // Method called by Field Of View slider
    public void ChangeFOV()
    { 
        OnFOVChanged?.Invoke();
    }

    // Method called by Previous Resolution button
    public void PreviousResolutionClick()
    {
        OnPreviousResolutionClicked?.Invoke();
    }

    // Method called by Next Resolution button
    public void NextResolutionClick()
    {
        OnNextResolutionClicked?.Invoke();
    }

    // Method called by VSync toggle
    public void ChangeVSync()
    {
        OnVSyncChanged?.Invoke();
    }

	// Method to display selection background for currently selected element
	public void selectElement(int index)
	{
        // If any element is currently selected, deactivate its selection background
		if (currentlySelectedElement != null)
		{
			currentlySelectedElement.selection.gameObject.SetActive(false);
		}

        // Set specified element as currently selected element and activate its selection background
		currentlySelectedElement = Elements[index];
		currentlySelectedElement.selection.gameObject.SetActive(true);

        // Find event system object and set specified element as selected element in event system by selecting an object based on type of element
        UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
        if (currentlySelectedElement is UIToggle)
        {
            var tmp = (UIToggle) currentlySelectedElement;
            eventSystem.SetSelectedGameObject(tmp.toggle.gameObject);
        }
        else if (currentlySelectedElement is UIResolution)
        {
            var tmp = (UIResolution) currentlySelectedElement;
            eventSystem.SetSelectedGameObject(tmp.gameObject);
        }
        else if (currentlySelectedElement is UISlider)
        {
            var tmp = (UISlider) currentlySelectedElement;
            eventSystem.SetSelectedGameObject(tmp.slider.gameObject);
        }
	}
}
