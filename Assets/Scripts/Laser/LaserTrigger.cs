using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTrigger : MonoBehaviour
{
    // Particle system component from laser object
    ParticleSystem laser;

    // Dictionary used for changing laser color based on which glass was touched by a particle
    Dictionary<string, Color32> glassColors;

    // Dictionaries used for changing object visibility and collision based on which type of object (from which dimension) a particle touched
    Dictionary<string, Color32> dimensionColors;
    Dictionary<string, Color32> nonDimensionColors;
    Dictionary<Color32, string> dimensionLayers;


    // Start is called before the first frame update
    void Start()
    {
        // Get laser's particle system
        laser = GetComponent<ParticleSystem>();

        // Initialise glass colors dictionary with tags used for glasses and color values used to change laser color
        glassColors = new Dictionary<string, Color32>();
        glassColors.Add("Red Glass", Color.red);
        glassColors.Add("Green Glass", Color.green);
        glassColors.Add("Blue Glass", Color.blue);

        // Initialise dimension colors dictionary with tags used for objects and color values used to change object visibility
        dimensionColors = new Dictionary<string, Color32>();
        dimensionColors.Add("Red Object", Color.red);
        dimensionColors.Add("Green Object", Color.green);
        dimensionColors.Add("Blue Object", Color.blue);

        // Initialise non-dimension colors dictionary with tags used for objects and color values used to change object visibility
        nonDimensionColors = new Dictionary<string, Color32>();
        nonDimensionColors.Add("Non-Red Object", Color.red);
        nonDimensionColors.Add("Non-Green Object", Color.green);
        nonDimensionColors.Add("Non-Blue Object", Color.blue);

        // Initialise non-dimension colors dictionary with color values and their respective dimension names
        dimensionLayers = new Dictionary<Color32, string>();
        dimensionLayers.Add(Color.red, "Red Dimension Object");
        dimensionLayers.Add(Color.green, "Green Dimension Object");
        dimensionLayers.Add(Color.blue, "Blue Dimension Object");
    }

    // A method called every time a laser particle triggers an object
    void OnParticleTrigger()
    {
        // List of laser particles involved in the trigger
        List<ParticleSystem.Particle> particles = new List<ParticleSystem.Particle>();

        // Number of particles involved in the Enter trigger and data of an object that was triggered
        int n = laser.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, particles, out var colliderData);

        // For every particle involved in the trigger (n always = 1 because of how the laser works)
        for (int i = 0; i < n; i++)
        {
            // Variable for storing currently iterated particle
            ParticleSystem.Particle p = particles[i];

            // If particle collided with exactly one object (should always happen, but better safe than sorry)
            if (colliderData.GetColliderCount(i) == 1)
            {
                // If particle collided with glass (glasses have their color set as their tag), set laser color to a color specified for this type of glass in dictionary
                if (glassColors.ContainsKey(colliderData.GetCollider(i, 0).gameObject.transform.parent.tag))
                {
                    p.startColor = glassColors[colliderData.GetCollider(i, 0).gameObject.transform.parent.tag];
                }
                // If particle collided with normal dimension object (parents of such objects have specific tags set that are specified in colors dictionary)
                else if (dimensionColors.ContainsKey(colliderData.GetCollider(i, 0).gameObject.transform.parent.tag))
                {
                    // Get dimension object collider's parent and all its children
                    var parent = colliderData.GetCollider(i, 0).gameObject.transform.parent;
                    var children = parent.GetComponentsInChildren<Transform>();

                    // If color of laser is compatible with object dimension's color
                    if (p.startColor.Equals(dimensionColors[parent.tag]))
                    {
                        // For each child of collider's parent
                        foreach (Transform child in children)
                        {
							// Get object's renderer
							var rend = child.GetComponent<MeshRenderer>();

							// If renderer is not null, change object's material to a version of this material that is visible in all dimensions (based on name)
							if (rend != null)
							{
								var name = rend.material.name.Replace("(Instance)", "");
								rend.material = Resources.Load("Materials/" + name + "Visible", typeof(Material)) as Material;
							}

                            // Change object's layer to default layer, so that object's collision is enabled
                            child.gameObject.layer = LayerMask.NameToLayer("Default");
                        }

                        // Change parent object's tag so it becomes a reverse dimension object
                        parent.tag = "Non-" + parent.tag;
                    }

                    // Set particle's remaining lifetime to 0, effectively destroying it
                    p.remainingLifetime = 0;
                }
                // If particle collided with reverse dimension object (not visible only in specified dimension)
                else if (nonDimensionColors.ContainsKey(colliderData.GetCollider(i, 0).gameObject.transform.parent.tag))
                {
                    // Get dimension object collider's parent and all its children
                    var parent = colliderData.GetCollider(i, 0).gameObject.transform.parent;
                    var children = parent.GetComponentsInChildren<Transform>();

                    // If color of laser is compatible with object dimension color
                    if (p.startColor.Equals(nonDimensionColors[parent.tag]))
                    {
                        // For each child of collider's parent
                        foreach (Transform child in children)
                        {
							// Get object's renderer
							var rend = child.GetComponent<MeshRenderer>();

							// If renderer is not null, change object's material to a version of this material that is not visible in its dimension (based on name)
							if (rend != null)
							{
								var name = rend.material.name.Replace("(Instance)", "").Replace(" Visible ", "");
								rend.material = Resources.Load("Materials/" + name, typeof(Material)) as Material;
							}

                            // Change object's layer to dimension object layer based on dimension color
                            if (dimensionLayers.ContainsKey(p.startColor))
                            {
                                child.gameObject.layer = LayerMask.NameToLayer(dimensionLayers[p.startColor]);
                            }
                        }

                        // Change parent object's tag so it becomes a dimension object
                        parent.tag = parent.tag.Replace("Non-", "");
                    }

                    // Set particle's remaining lifetime to 0, effectively destroying it
                    p.remainingLifetime = 0;
                }
                // If particle collided with any other object with trigger, set particle's remaining lifetime to 0
				else
				{
					p.remainingLifetime = 0;
				}
			}

            // Replace iterated particle with modified particle on the list
            particles[i] = p;
        }

        // Replace currently rendered particles involved in the trigger with modified particles
        laser.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, particles);
    }
}
