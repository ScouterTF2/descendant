using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPickup : MonoBehaviour
{
	// Reference to state machine
	PlayerController playerController;

	// Start is called before the first frame update
	void Start()
    {
		// Find object with state machine component
		playerController = FindObjectOfType<PlayerController>();
	}
	
	void OnTriggerEnter(Collider other)
	{
		// If Player collided with laser pickup
		if (other.CompareTag("Player"))
		{
			// Enable laser shoot component of player
			playerController.EnableLaser();

			// Destroy laser pickup
			Destroy(gameObject);
		}
	}
}
