using TMPro;
using UnityEngine;
using UnityEngine.Events;

// Message view class, passes button events
public class MessageView : BaseView
{
	// Events to attach to
	public UnityAction OnCloseClicked;

	// Reference to message UI object
	[SerializeField] public TextMeshProUGUI message;

	// Reference to message indicators
	[SerializeField] public TextMeshProUGUI skipIndicator;
	[SerializeField] public TextMeshProUGUI endIndicator;

	// Method called by Close button
	public void CloseClick()
	{
		// Invoke action set to Close button
		OnCloseClicked?.Invoke();
	}
}
