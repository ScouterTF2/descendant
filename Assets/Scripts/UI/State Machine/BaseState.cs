// Base state script implementation, uses virtual methods below to call state when it needs to prepare itself for operating, updating or being destroyed
public abstract class BaseState
{
    // Reference to state machine
    public StateMachine owner;

    // Method called to prepare state to operate - same as Unity's Start()
    public virtual void PrepareState() { }

    // Method called to destroy state - same as Unity's OnDestroy(), but here it might be important
    public virtual void DestroyState() { }

    // Method called to update state on every frame - same as Unity's Update()
    public virtual void UpdateState() { }
}