using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayerHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Number of music players in scene
        int players = FindObjectsOfType<MusicPlayerHandler>().Length;

        // If there's more than one player in the scene, destroy this object
        if (players > 1)
        {
            Destroy(gameObject);
        }

        // Set music volume according to music volume in player prefs
        SetVolume();
    }

    public void SetVolume()
    {
        // Get audio source component
        AudioSource audioSource = gameObject.GetComponent<AudioSource>();
        
        // Change volume of music player's audio source to music volume set in player prefs
        audioSource.volume = PlayerPrefs.GetFloat("musicVolume");
	}
}
