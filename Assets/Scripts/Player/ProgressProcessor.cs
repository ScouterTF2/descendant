using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ProgressProcessor : MonoBehaviour
{
	// Reference to save data object
	public SaveData saveData;

	// Path to save file
	string filePath;


	void Start()
	{
		// Set default path and name of save file
		filePath = Path.Combine(Application.persistentDataPath, "SaveFile.json");

		// If file in that path exists, read it
		if (File.Exists(filePath))
		{
			ReadSaveFile();
		}
		// If not, initialise save data and write deserialized save data object to save file
		else
		{
			saveData = new SaveData { currentLevel = 0, levelAchieved = 0};
			File.WriteAllText(filePath, JsonUtility.ToJson(saveData));
		}
	}

	public void UpdateCurrentLevel(int level)
	{
		// Set new current level to save data and write deserialized save data object to save file
		saveData.currentLevel = level;
		File.WriteAllText(filePath, JsonUtility.ToJson(saveData));
	}

	public void UpdateTotalProgress(int level)
	{
		// Set new achieved (completed) level to save data and write deserialized save data object to save file
		saveData.levelAchieved = level;
		File.WriteAllText(filePath, JsonUtility.ToJson(saveData));
	}

	void ReadSaveFile()
	{
		// Read text from save file - it should be a deserialized SaveData object - and serialize it into a SaveData object
		var tmp = File.ReadAllText(filePath);
		saveData = JsonUtility.FromJson<SaveData>(tmp);
	}
}
