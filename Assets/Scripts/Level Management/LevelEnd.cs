using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEnd : MonoBehaviour
{
	// Reference to player's controller
	PlayerController playerController;

	// Reference to UI state machine
	[SerializeField] StateMachine stateMachine;

	// Start is called before the first frame update
	void Start()
	{
		// Get player's controller
		playerController = gameObject.GetComponent<PlayerController>();
	}

	void OnTriggerEnter(Collider other)
	{
		// If player collided with exit area
		if (other.gameObject == playerController.levelManager.ExitArea)
		{
			// If previously unfinished level has been finished 
			if (stateMachine.CurrentLevel > playerController.gameObject.GetComponent<ProgressProcessor>().saveData.levelAchieved)
			{
				// Set achieved level in save data to current level
				playerController.gameObject.GetComponent<ProgressProcessor>().UpdateTotalProgress(stateMachine.CurrentLevel);
			}

			// Set current state to Loading Level state with next level set as level to load and with disabling player's movement, laser and sound in the state
			stateMachine.ChangeState(new LoadingLevelState { levelToLoad = playerController.levelManager.NextLevelNumber, jumpFromGame = true });
		}
	}
}
