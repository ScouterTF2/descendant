using UnityEngine;
using UnityEngine.SceneManagement;

// Pause state, implements pause menu as different state than game state
public class PauseState : BaseState
{
    // Variables used for enabling player's movement and sound when changing state to Game and disabling it when NOT changing state from Options
    public bool jumpToGame = false;
    public bool jumpFromOptions = false;

	// Reference to number of currently selected button from buttons list
	int currentButtonNumber;

    public override void PrepareState()
    {
        base.PrepareState();

        // Stop time in game
        Time.timeScale = 0;

        // Attach functions to view events
        owner.UI.PauseView.OnMenuClicked += MenuClicked;
        owner.UI.PauseView.OnOptionsClicked += OptionsClicked;
        owner.UI.PauseView.OnResumeClicked += ResumeClicked;

        // If Options was not previous state, disable player's sound
        if (!jumpFromOptions)
        {
            owner.PlayerController.DisableSound();
        }

		// Set first button in list as current button and select it
		currentButtonNumber = 0;
		owner.UI.PauseView.selectButton(currentButtonNumber);

        // Show Pause view
        owner.UI.PauseView.ShowView();
    }

	public override void DestroyState()
    {
		// Hide Pause view
		owner.UI.PauseView.HideView();

		// If state is being changed to Game, enable player's sound and start time in game
		if (jumpToGame)
        {
            owner.PlayerController.EnableSound();

            Time.timeScale = 1;
        }
		// Otherwise, drop item from player's item arm if there is one in it
		else
		{
			if (owner.PlayerController.ItemPickup.Item != null)
            {
                owner.PlayerController.ItemPickup.DropItem();
            }
		}

        // Detach functions from view events
        owner.UI.PauseView.OnMenuClicked -= MenuClicked;
        owner.UI.PauseView.OnOptionsClicked -= OptionsClicked;
        owner.UI.PauseView.OnResumeClicked -= ResumeClicked;

		// Find event system object and remove its button/element selection
		UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		eventSystem.SetSelectedGameObject(null);

        base.DestroyState();
    }

	public override void UpdateState()
	{
		base.UpdateState();

		// Handle states of UI key presses
		HandleDownClicked();
		HandleUpClicked();
		HandleCancelClicked();
	}

	void HandleDownClicked()
	{
		// If Menu Control Down button is pressed
		if (Input.GetButtonDown("MenuControlDown"))
		{
			// If current button is the last button in list, set first button in list as current button and select it
			if (currentButtonNumber == owner.UI.PauseView.Buttons.Count - 1)
			{
				currentButtonNumber = 0;
				owner.UI.PauseView.selectButton(currentButtonNumber);
			}
			// Otherwise, set next button in list as current button and select it
			else
			{
				currentButtonNumber++;
				owner.UI.PauseView.selectButton(currentButtonNumber);
			}
		}
	}

	void HandleUpClicked()
	{
		// If Menu Control Up button is pressed
		if (Input.GetButtonDown("MenuControlUp"))
		{
			// If current button is the first button in list, set last button in list as current button and select it
			if (currentButtonNumber == 0)
			{
				currentButtonNumber = owner.UI.PauseView.Buttons.Count - 1;
				owner.UI.PauseView.selectButton(currentButtonNumber);
			}
			// Otherwise, set previous button in list as current button and select it
			else
			{
				currentButtonNumber--;
				owner.UI.PauseView.selectButton(currentButtonNumber);
			}
		}
	}

	void HandleCancelClicked()
	{
		// If Cancel button is pressed, invoke resume function
		if (Input.GetButtonDown("Cancel"))
		{
			ResumeClicked();
		}
	}

	// Function called when Resume button is clicked in Pause view
    void ResumeClicked()
    {
        // Allow enabling player's sound
        jumpToGame = true;

        // Set current state to Game state
        owner.ChangeState(new GameState());
    }

	// Function called when Options button is clicked in Pause view
    void OptionsClicked()
    {
        // Set current state to Options state
        owner.ChangeState(new OptionsState{ returnToState = OptionsState.ReturnToStates.Pause});
    }

    // Function called when Menu button is clicked in Pause view
    void MenuClicked()
    {
        // Unload level
        SceneManager.UnloadSceneAsync("Level" + owner.CurrentLevel);

        // Start time in game
        Time.timeScale = 1;

        // Set current state to Menu state
        owner.ChangeState(new MenuState());
    }
}