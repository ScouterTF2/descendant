using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Set source volume according to effects volume in player prefs
        SetVolume();
    }

    public void SetVolume()
    {
        // Get audio source component
        AudioSource audioSource = gameObject.GetComponent<AudioSource>();
        
        // Change volume of audio source to effects volume set in player prefs
        audioSource.volume = PlayerPrefs.GetFloat("effectsVolume");
	}
}
